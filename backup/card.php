@extends('public.master2')

@section('content')

 <style>
.scroll{
  width: 100%;
  padding: 10px;
  overflow: scroll;
  height: 1000px;
  
  /*script tambahan khusus untuk IE */
  scrollbar-face-color: #CE7E00; 
  scrollbar-shadow-color: #FFFFFF; 
  scrollbar-highlight-color: #6F4709; 
  scrollbar-3dlight-color: #11111; 
  scrollbar-darkshadow-color: #6F4709; 
  scrollbar-track-color: #FFE8C1; 
  scrollbar-arrow-color: #6F4709;
}
        
.column {
    float: left;
    width: 50%;
}

.column2 {
    float: left;
    width: 50%;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}   
.column {
    float: left;
}

.left {
    width: 25%;
}

.right {
    width: 75%;
}
@media (max-width: 600px) {
    .column {
        width: 100%;
    }
}

</style>

 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            



<div class="row">
   <div class="col-lg-6">
    <div class="scroll">
    
        <div class="box">
            <header>
                <h5>Wizard with Validation</h5>
            </header>
            <div id="div-2" class="body">
                <form id="wizardForm" method="post" action="" class="form-horizontal wizardForm">

                    <fieldset class="step" id="first">
                        <h4 class="text-danger pull-right">Product: ISLAMIC (Pre Approved)</h4>
                        <div class="clearfix"></div>
                
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">SC Channel / Branch<sup>*</sup></label>
                            <div class="col-lg-2">
                                <select name="sc_channel" id="sc_channel" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($sc_channel as $sc_channel)
                                        <option value="{{$sc_channel->sc_code }}">{{ $sc_channel->sc_code }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-6">
                                 <select name="sc_branch" id="sc_branch" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($sc_branch as $sc_branch)
                                        <option value="{{$sc_branch->source_code_branche_code }}">{{ $sc_branch->source_code_branche_code }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-param col-lg-4">Agent Type / Agent No<sup>*</sup></label>
                            <div class="col-lg-2">
                                <select name="agent_type" id="agent_type" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($agent_type as $agent_type)
                                        <option value="{{$agent_type->agent_type_code }}">{{ $agent_type->agent_type_code }}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select name="agent_no" id="agent_no" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Introducer's / Agent Name</label>

                            <div class="col-lg-8">
                                <input type="text" name="agent_name" id="agent_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">eCosway Introducer's ID No</label>

                            <div class="col-lg-8">
                                <input type="text" name="ecosway" id="ecosway" class="form-control">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="parameter" class="control-param col-lg-4">Card Delivery<sup>*</sup></label>

                            <div class="col-lg-8">
                                <select name="card_delivery" id="card_delivery" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($card_delivery as $card_delivery)
                                        <option value="{{$card_delivery->card_delivery_code }}">{{ $card_delivery->card_delivery_desc }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Card Collection<sup>*</sup></label>
                            <div class="col-lg-8">
                                <select name="card_collection" id="card_collection" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($card_collection as $card_collection)
                                        <option value="{{$card_collection->card_collection_code }}">{{ $card_collection->card_collection_desc }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Collection Branch (State)<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="card_branch_state" id="card_branch_state" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($card_branch_state as $card_branch_state)
                                        <option value="{{$card_branch_state->card_branch_state_code }}">{{ $card_branch_state->card_branch_state_desc }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Collection Branch<sup>*</sup></label>
                            <div class="col-lg-8">
                                <select name="card_branch" id="card_branch" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Plastic Data E4</label>
                            <div class="col-lg-8">
                                <select name="plastic_data" id="plastic_data" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($plastic_data as $plastic_data)
                                        <option value="{{$plastic_data->plastic_data_code }}">{{ $plastic_data->plastic_data_desc }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                          <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Business Owner ID</label>
                            <div class="col-lg-8">
                                <select name="business_owner" id="business_owner" class="form-control chzn-select">
                                    <option selected="">PLEASE SELECT</option>
                                    @foreach ($business_owner as $business_owner)
                                        <option value="{{$business_owner->id }}">{{ $business_owner->business_owner_desc }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                          <div class="form-group">
                            <label for="server_user" class="control-label col-lg-4">Date Applied</label>

                            <div class="col-lg-8">
                                <input type="date" name="date_applied" id="date_applied" class="form-control"  placeholder="ddmmyy (010118)">
                            </div>
                           
                        </div>
                          <div class="form-group">
                            <label for="server_user" class="control-label col-lg-4">Ambak Protector (Y/N)<sup>*</sup></label>

                            <div class="col-lg-8">
                                <input type="text" name="ambank_protector" id="ambank_protector" class="form-control">
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="step" id="second">
                        <h4 class="text-danger pull-right">Product: ISLAMIC (Pre Approved)</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">SC Channel / Branch<sup>*</sup></label>

                            <div class="col-lg-2">
                                <input type="text" name="sc_channel" id="a" class="form-control">
                            </div>
                             <div class="col-lg-6">
                                <input type="text" name="branch" id="s" class="form-control">
                            </div>
                        </div>
                           <table class="table table-bordered orang-dihubungi">
                        <tr>
                            <td colspan="8">
                                <a id="add-orang-dihubungi" class="{{config('params.add_btn')}} pull-right mb-3  text-white"><i class="{{config('params.add_icon')}}"></i>
                                {{ config('params.add') }}</a>
                            </td>
                        </tr>  
                                <thead></thead>
                                    <th></th>
                                    <th width="10%"><b>Prg Code</b></th>
                                    <th><b>Card Tyoe</b></th>
                                    <th><b>Gift</b></th>
                                    <th><b>Zing Ind (Y/N)</b></th>
                                    <th></th>
                                </thead>
                                
                                 <tr>
                                    <td></td>
                                    <td>
                                        <select name="program_code[]" id="program_code" class="form-control chzn-select">
                                            <option selected="">PLEASE SELECT</option>
                                                @foreach ($programme_code as $programme_code)
                                                    <option value="{{$programme_code->id }}">{{ $programme_code->programme_code_desc }}</option>
                                                @endforeach
                                        </select>
                                    </td>
                                    <td><b>Card Tyoe</b></td>
                                    <td><b>Gift</b></td>
                                    <td><b>Zing Ind (Y/N)</b></td>
                                    <td></td>
                                </tr>
                                  
                            </table>
                    </fieldset>
                     <fieldset class="step" id="third">
                        <h4 class="text-danger pull-right">Product: ISLAMIC (Pre Approved)</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">SC Channel / Branch<sup>*</sup></label>

                            <div class="col-lg-2">
                                <input type="text" name="sc_channel"  class="form-control">
                            </div>
                             <div class="col-lg-6">
                                <input type="text" name="branch" id="branch" class="form-control">
                            </div>
                        </div>
                          <table class="table table-bordered orang-dihubungi">
                                <thead></thead>
                                    <th></th>
                                    <th><b>Prg Code</b></th>
                                    <th><b>Card Tyoe</b></th>
                                    <th><b>Gift</b></th>
                                    <th><b>Zing Ind (Y/N)</b></th>
                                </thead>
                                 @for($i=1;$i<=8;$i++)
                                 <tr>
                                    <td>{{$i}}</td>
                                    <td><b>Prg Code</b></td>
                                    <td><b>Card Tyoe</b></td>
                                    <td><b>Gift</b></td>
                                    <td><b>Zing Ind (Y/N)</b></td>
                                </tr>
                                   @endfor
                            </table>
                    </fieldset>

                    <fieldset class="step" id="fourth">
                        <h4 class="text-danger pull-right">Product: ISLAMIC (Pre Approved)</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                <select name="salutation" id="salutation" class="form-control chzn-select">
                                    @foreach ($id_type as $id_type)
                                        <option value="{{$id_type->id_type_code }}">{{ $id_type->id_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             
                             <div class="col-lg-2">
                                <label for="sc_channel" class="">ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                <input type="text" name="id_no" id="id_no" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Alternate ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                <input type="text" name="old_ic" id="old_ic" class="form-control" placeholder="OLD IC">
                            </div>
                              <div class="col-lg-2">
                                <label for="sc_channel" >Alternate ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="id_no_alt" id="id_no_alt" class="form-control">
                            </div>

                        </div>

                        
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Customer Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="customer_name" id="customer_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Salutation<sup>*</sup></label>
                            <div class="col-lg-8">
                                  <select name="salutation"  class="form-control chzn-select">
                                    @foreach ($salutation as $salutation)
                                        <option value="{{$salutation->salutation_code }}">{{ $salutation->salutation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Name On Card<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name_on_card" id="name_on_card" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Sex<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="sexs_code" id="sexs_code" class="form-control">
                                    @foreach ($sex as $sex)
                                        <option value="{{$sex->sexs_code }}">{{ $sex->sexs_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Race<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="race_code" id="race_code" class="form-control chzn-select">
                                    @foreach ($race as $race)
                                        <option value="{{$race->race_code }}">{{ $race->race_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Religion<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="religion_code" id="religion_code" class="form-control chzn-select">
                                    @foreach ($religion as $religion)
                                        <option value="{{$religion->religion_code }}">{{ $religion->religion_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Ethnic<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="ethnic_code" id="ethnic_code" class="form-control chzn-select">
                                    @foreach ($ethnic as $ethnic)
                                        <option value="{{$ethnic->ethnic_code }}">{{ $ethnic->ethnic_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Nationality<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="nationality_code" id="nationality_code" class="form-control chzn-select">
                                    @foreach ($nationality as $nationality)
                                        <option value="{{$nationality->nationality_code }}">{{ $nationality->nationality_code_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Citizenship<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="citizenship_code" id="citizenship_code" class="form-control chzn-select">
                                    @foreach ($citizenship as $citizenship)
                                        <option value="{{$citizenship->citizenship_code }}">{{ $citizenship->citizenship_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Marital Status<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="marital_code" id="marital_code" class="form-control chzn-select">
                                    @foreach ($marital as $marital)
                                        <option value="{{$marital->marital_code }}">{{ $marital->marital_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Education Level<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="education_code" id="education_code" class="form-control chzn-select">
                                    @foreach ($education as $education)
                                        <option value="{{$education->education_code }}">{{ $education->education_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Ambank Staff<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="ambank_staff" id="ambank_staff" class="form-control chzn-select">
                                    @foreach ($ambank_staff as $ambank_staff)
                                        <option value="{{$ambank_staff->ambank_staff_code }}">{{ $ambank_staff->ambank_staff_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">VIP Customer<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="vip_code" id="vip_code" class="form-control chzn-select">
                                    @foreach ($vip as $vip)
                                        <option value="{{$vip->vip_code }}">{{ $vip->vip_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Mother's Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="mother_name" id="mother_name" class="form-control">
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="step" id="fifth">
                        <h4 class="text-warning pull-right">Table Settings</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Mailing Address<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="mailing_address" id="mailing_address" class="form-control">
                                    @foreach ($mailing_address as $mailing_address)
                                        <option value="{{$mailing_address->mailing_address_code }}">{{ $mailing_address->mailing_address_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Home Address</label>

                            <div class="col-lg-8">
                                   <input type="text" name="address_1" id="address_1" class="form-control">
                            </div>
                             <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_2" id="address_2" class="form-control">
                            </div>
                              <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_3" id="address_3" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">Postcode<sup>*</sup></label>

                            <div class="col-lg-3">
                                <input type="text" name="postcode" id="postcode" class="form-control" placeholder="OLD IC">
                            </div>
                              <div class="col-lg-2">
                                <label for="parameter" >City<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="city" id="city" class="form-control">
                            </div>

                        </div>

                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">State</label>

                            <div class="col-lg-8">
                                   <input type="text" name="state" id="state" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Country</label>

                            <div class="col-lg-8">
                                   <input type="text" name="country" id="country" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Home tel.no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="home_phone" id="home_phone" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="home_phone2" id="home_phone2" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Mobile phone no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="mobile_phone" id="mobile_phone" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="mobile_phone2" id="mobile_phone2" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Email</label>

                            <div class="col-lg-8">
                                   <input type="email" name="email" id="email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Residence Type<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="residence_type" id="residence_type" class="form-control">
                                    @foreach ($residence as $residence)
                                        <option value="{{$residence->residence_code }}">{{ $residence->residence_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Years of Stay</label>

                            <div class="col-lg-3">
                                   <input type="text" name="year_stay" id="year_stay" class="form-control" placeholder="Year">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="month_stay" id="month_stay" class="form-control" placeholder="Month">
                            </div>
                        </div>
                    </fieldset>

                     <fieldset class="step" id="sixth">
                        <h4 class="text-warning pull-right">Table Settings</h4>
                        <div class="clearfix"></div>
                        
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Permanent Address<sup>*</sup></label>

                            <div class="col-lg-8">
                                   <input type="text" name="address_1_per" id="address_1_per" class="form-control">
                            </div>
                             <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_2_per" id="address_2_per" class="form-control">
                            </div>
                              <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_3_per" id="address_3_per" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">Postcode<sup>*</sup></label>

                            <div class="col-lg-3">
                                <input type="text" name="postcode_per" id="postcode_per" class="form-control" placeholder="OLD IC">
                            </div>
                              <div class="col-lg-2">
                                <label for="parameter" >City<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="city_per" id="city_per" class="form-control">
                            </div>

                        </div>

                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">State</label>

                            <div class="col-lg-8">
                                   <input type="text" name="state_per" id="state_per" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Country</label>

                            <div class="col-lg-8">
                                   <input type="text" name="country_per" id="country_per" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Home tel.no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="home_phone_per" id="home_phone_per" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="home_phone2_per" id="home_phone2_per" class="form-control">
                            </div>
                        </div>
                       

                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Years of Stay</label>

                            <div class="col-lg-3">
                                   <input type="text" name="year_stay_per" id="year_stay_per" class="form-control" placeholder="Year">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="month_stay_per" id="month_stay_per" class="form-control" placeholder="Month">
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="step" id="seventh">
                        <h4 class="text-warning pull-right">Table Settings</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Employment Type<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="employement_type" id="employement_type" class="form-control">
                                    @foreach ($employement_type as $employement_type)
                                        <option value="{{$employement_type->employement_type_code }}">{{ $employement_type->employement_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Employer Name</label>
                            <div class="col-lg-8">
                                   <input type="text" name="employer_name" id="employer_name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">Department</label>

                            <div class="col-lg-3">
                                <input type="text" name="department" id="department" class="form-control" placeholder="Department">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Employer Address</label>

                            <div class="col-lg-8">
                                   <input type="text" name="address_emp" id="address_emp" class="form-control">
                            </div>
                             <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_emp2" id="address_emp2" class="form-control">
                            </div>
                              <label for="table_collation" class="control-label col-lg-4"></label>
                             <div class="col-lg-8">
                                   <input type="text" name="address_emp3" id="address_emp3" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sc_channel" class="control-label col-lg-4">Postcode<sup>*</sup></label>

                            <div class="col-lg-3">
                                <input type="text" name="postcode_emp" id="postcode_emp" class="form-control" placeholder="OLD IC">
                            </div>
                              <div class="col-lg-2">
                                <label for="parameter" >City<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="city_emp" id="city_emp" class="form-control">
                            </div>

                        </div>

                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">State</label>

                            <div class="col-lg-8">
                                   <input type="text" name="state_emp" id="state_emp" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Country</label>

                            <div class="col-lg-8">
                                   <input type="text" name="country_emp" id="country_emp" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Office tel.no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="office_phone" id="office_phone" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="office_phone2" id="office_phone2" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Fax no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="fax_no" id="fax_no" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="fax_no2" id="fax_no2" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Email</label>

                            <div class="col-lg-8">
                                   <input type="email" name="email_emp" id="email_emp" class="form-control">
                            </div>
                        </div>
                    </fieldset>

                      <fieldset class="step" id="eight">
                        <h4 class="text-warning pull-right">Table Settings</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Nature of Business<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="naturebusiness" id="naturebusiness" class="form-control">
                                    @foreach ($naturebusiness as $naturebusiness)
                                        <option value="{{$naturebusiness->nature_of_business_code }}">{{ $naturebusiness->nature_of_business_code_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Occupation<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="occupation" id="occupation" class="form-control">
                                    @foreach ($occupation as $occupation)
                                        <option value="{{$occupation->occupation_code }}">{{ $occupation->occupation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Occupation Description</label>
                            <div class="col-lg-8">
                                   <input type="text" name="occupation_desc" id="occupation_desc" class="form-control">
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Job Sector<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="job_sector" id="job_sector" class="form-control">
                                    @foreach ($job_sector as $job_sector)
                                        <option value="{{$job_sector->job_sector_code }}">{{ $job_sector->job_sector_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Designation</label>
                            <div class="col-lg-8">
                                 <select name="designation" id="designation" class="form-control">
                                    @foreach ($designation as $designation)
                                        <option value="{{$designation->designation_code }}">{{ $designation->designation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="label" class="control-label col-lg-4">Date Joined</label>
                            <div class="col-lg-3">
                                  <input type="text" name="date_joined" id="date_joined" class="form-control">
                            </div>
                             <div class="col-lg-2">
                                <label for="label">Years of employement<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <div class="col-lg-2">
                                   <input type="text" name="year_emp" id="year_emp" class="form-control">
                                    </div>
                             <div class="col-lg-2">
                                   <input type="text" name="month_emp" id="month_emp" class="form-control">
                            </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="label" class="control-label col-lg-4">Monthly Income+</label>
                            <div class="col-lg-3">
                                  <input type="text" name="monthly_income" id="monthly_income" class="form-control">
                            </div>
                             <div class="col-lg-2">
                                <label for="label">Annual Income+<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                <input type="text" name="annual_income" id="annual_income" class="form-control">
                            </div>
                        </div>
                        <br><br>
                        PREVIOUS EMPLOYEMENT
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Name of Company<sup>*</sup></label>

                            <div class="col-lg-4">
                                   <input type="text" name="name_company" id="name_company" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Job Sector<sup>*</sup></label>
                            <div class="col-lg-8">
                                 <select name="job_sector_prev" id="job_sector_prev" class="form-control">
                                    @foreach ($job_sector as $job_sectors)
                                        <option value="{{$job_sectors->id }}">{{ $job_sectors->job_sector_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Designation</label>
                            <div class="col-lg-8">
                                 <select name="designation_prev" id="designation_prev" class="form-control">
                                    @foreach ($designation as $designations)
                                        <option value="{{$designations->designation_code }}">{{ $designations->designation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="label" class="control-label col-lg-4">Date Joined</label>
                            <div class="col-lg-3">
                                  <input type="text" name="date_joined_prev" id="date_joined_prev" class="form-control">
                            </div>
                             <div class="col-lg-2">
                                <label for="label">Years of employement<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <div class="col-lg-2">
                                   <input type="text" name="year_emp_prev" id="year_emp_prev" class="form-control">
                                    </div>
                             <div class="col-lg-2">
                                   <input type="text" name="month_emp_prev" id="month_emp_prev" class="form-control">
                            </div>
                            </div>
                        </div>
                    </fieldset>
                      <fieldset class="step" id="ninth">
                        <h4 class="text-danger pull-right">Spouse Info / Personal Reference</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                <select name="id_type_spo" id="id_type_spo" class="form-control chzn-select">
                                    @foreach ($id_type as $id_type)
                                        <option value="{{$id_type->id_type_code }}">{{ $id_type->id_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             
                             <div class="col-lg-2">
                                <label for="sc_channel" class="">ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                <input type="text" name="id_no_spo" id="id_no_spo" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Alternate ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                 <select name="alternate_id_type_spo" id="alternate_id_type_spo" class="form-control chzn-select">
                                    @foreach ($id_type as $id_type)
                                        <option value="{{$id_type->id_type_code }}">{{ $id_type->id_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                              <div class="col-lg-2">
                                <label for="sc_channel" >Alternate ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="id_no_alt_spo" id="id_no_alt_spo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name_spo" id="name_spo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Relationship<sup>*</sup></label>
                            <div class="col-lg-8">
                                  <select name="relationship_spo"  class="form-control chzn-select">
                                    @foreach ($relationship as $relationship)
                                        <option value="{{$relationship->relationship_code }}">{{ $relationship->relationship_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Occupation</label>
                            <div class="col-lg-8">
                                 <select name="occupation_spo" id="occupation_spo" class="form-control">
                                    @foreach ($occupation as $occupation)
                                        <option value="{{$occupation->occupation_code }}">{{ $occupation->occupation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Other Occupation</label>
                            <div class="col-lg-8">
                                <input type="text" name="other_occupation" id="other_occupation" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Office phone no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="office_phone_spo" id="office_phone_spo" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="office_phone_spo2" id="office_phone_spo2" class="form-control">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Home tel.no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="home_phone_spo" id="home_phone_spo" class="form-control">
                            </div>
                             <div class="col-lg-5">
                                   <input type="text" name="home_phone_spo2" id="home_phone_spo2" class="form-control">
                            </div>
                        </div>
                       
                    </fieldset>
                      <fieldset class="step" id="tenth">
                        <h4 class="text-danger pull-right">Supplementrari Information</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                <select name="id_type_sup" id="id_type_sup" class="form-control chzn-select">
                                    @foreach ($id_type as $id_type)
                                        <option value="{{$id_type->id_type_code }}">{{ $id_type->id_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             
                             <div class="col-lg-2">
                                <label for="sc_channel" class="">ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                <input type="text" name="id_no_sup" id="id_no_sup" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Alternate ID Type<sup>*</sup></label>

                            <div class="col-lg-3">
                                <select name="alternate_id_type_sup" id="alternate_id_type_sup" class="form-control chzn-select">
                                    @foreach ($id_type as $id_type)
                                        <option value="{{$id_type->id_type_code }}">{{ $id_type->id_type_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                              <div class="col-lg-2">
                                <label for="sc_channel" >Alternate ID No<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <input type="text" name="id_no_alt_sup" id="id_no_alt_sup" class="form-control">
                            </div>

                        </div>

                        
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Customer Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="customer_name_sup" id="customer_name_sup" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Salutation<sup>*</sup></label>
                            <div class="col-lg-8">
                                  <select name="salutation_sup"  class="form-control chzn-select">
                                    @foreach ($salutation as $salutation)
                                        <option value="{{$salutation->salutation_code }}">{{ $salutation->salutation_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Name On Card<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name_on_card" id="name_on_card_sup" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Sex<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="sexs_sup" id="sexs_sup" class="form-control">
                                    @foreach ($sex as $sex)
                                        <option value="{{$sex->sexs_code }}">{{ $sex->sexs_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Race<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="race_sup" id="race_sup" class="form-control chzn-select">
                                    @foreach ($race as $race)
                                        <option value="{{$race->race_code }}">{{ $race->race_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Religion<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="religion_sup" id="religion_sup" class="form-control chzn-select">
                                    @foreach ($religion as $religion)
                                        <option value="{{$religion->religion_code }}">{{ $religion->religion_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Ethnic<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="ethnic_sup" id="ethnic_sup" class="form-control chzn-select">
                                    @foreach ($ethnic as $ethnic)
                                        <option value="{{$ethnic->ethnic_code }}">{{ $ethnic->ethnic_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Nationality<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="nationality_sup" id="nationality_sup" class="form-control chzn-select">
                                    @foreach ($nationality as $nationality)
                                        <option value="{{$nationality->nationality_code }}">{{ $nationality->nationality_code_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Citizenship<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="citizenship_sup" id="citizenship_sup" class="form-control chzn-select">
                                    @foreach ($citizenship as $citizenship)
                                        <option value="{{$citizenship->citizenship_code }}">{{ $citizenship->citizenship_desc }}</option>
                                        @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Marital Status<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="marital_sup" id="marital_sup" class="form-control chzn-select">
                                    @foreach ($marital as $marital)
                                        <option value="{{$marital->marital_code }}">{{ $marital->marital_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">Education Level<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="education_sup" id="education_sup" class="form-control chzn-select">
                                    @foreach ($education as $education)
                                        <option value="{{$education->education_code }}">{{ $education->education_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Ambank Staff<sup>*</sup></label>
                            <div class="col-lg-3">
                                  <select name="ambank_staff_sup" id="ambank_staff_sup" class="form-control chzn-select">
                                    @foreach ($ambank_staff as $ambank_staff)
                                        <option value="{{$ambank_staff->ambank_staff_code }}">{{ $ambank_staff->ambank_staff_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">VIP Customer<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                 <select name="vip_code_sup" id="vip_code_sup" class="form-control chzn-select">
                                    @foreach ($vip as $vip)
                                        <option value="{{$vip->vip_code }}">{{ $vip->vip_desc }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Mother's Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="mother_name_sup" id="mother_name_sup" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="step" id="last">
                        <h4 class="text-primary pull-right">User Settings</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="username" class="control-label col-lg-4">Username</label>

                            <div class="col-lg-8">
                                <input type="text" name="username" id="username" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usermail" class="control-label col-lg-4">E-mail</label>

                            <div class="col-lg-8">
                                <input type="text" name="usermail" id="usermail" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pass" class="control-label col-lg-4">User Password</label>

                            <div class="col-lg-8">
                                <input type="password" name="pass" id="pass" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pass2" class="control-label col-lg-4">Confirm Password</label>

                            <div class="col-lg-8">
                                <input type="password" name="pass2" id="pass2" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-8">
                                <label class="checkbox">
                                    <input type="checkbox" class="form-control"> Remember me
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-actions">
                        <input class="navigation_button btn" id="back" value="Back" type="reset"/>
                        <input class="navigation_button btn btn-primary" id="next" value="Next"
                               type="submit"/>
                    </div>
                </form>
                <div id="data"></div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
  <div class="col-lg-6">
    <div class="scroll">
        

       <object height="100%"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf?#zoom=85&scrollbar=0&toolbar=0&navpanes=0" id="pdf_content">
    <p>Insert your error message here, if the PDF cannot be displayed.</p>
  </object>

    </div>
  </div>
<!--<iframe src="" border="0" framspacing="0" marginheight="0" marginwidth="0" vspace="0" hspace="0" frameborder="0" ></iframe>-->
</div>
<!-- /.row -->

                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.outer -->
                </div>
                <!-- /#content -->

                    <div id="right" class="onoffcanvas is-right is-fixed bg-light" aria-expanded=false>
                        <a class="onoffcanvas-toggler" href="#right" data-toggle=onoffcanvas aria-expanded=false></a>
                        <br>
                        <br>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Warning!</strong> Best check yo self, you're not looking too good.
                        </div>
                        <!-- .well well-small -->
                        <div class="well well-small dark">
                            <ul class="list-unstyled">
                                <li>Visitor <span class="inlinesparkline pull-right">1,4,4,7,5,9,10</span></li>
                                <li>Online Visitor <span class="dynamicsparkline pull-right">Loading..</span></li>
                                <li>Popularity <span class="dynamicbar pull-right">Loading..</span></li>
                                <li>New Users <span class="inlinebar pull-right">1,3,4,5,3,5</span></li>
                            </ul>
                        </div>
                        <!-- /.well well-small -->
                        <!-- .well well-small -->
                        <div class="well well-small dark">
                            <button class="btn btn-block">Default</button>
                            <button class="btn btn-primary btn-block">Primary</button>
                            <button class="btn btn-info btn-block">Info</button>
                            <button class="btn btn-success btn-block">Success</button>
                            <button class="btn btn-danger btn-block">Danger</button>
                            <button class="btn btn-warning btn-block">Warning</button>
                            <button class="btn btn-inverse btn-block">Inverse</button>
                            <button class="btn btn-metis-1 btn-block">btn-metis-1</button>
                            <button class="btn btn-metis-2 btn-block">btn-metis-2</button>
                            <button class="btn btn-metis-3 btn-block">btn-metis-3</button>
                            <button class="btn btn-metis-4 btn-block">btn-metis-4</button>
                            <button class="btn btn-metis-5 btn-block">btn-metis-5</button>
                            <button class="btn btn-metis-6 btn-block">btn-metis-6</button>
                        </div>
                        <!-- /.well well-small -->
                        <!-- .well well-small -->
                        <div class="well well-small dark">
                            <span>Default</span><span class="pull-right"><small>20%</small></span>
                        
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-info" style="width: 20%"></div>
                            </div>
                            <span>Success</span><span class="pull-right"><small>40%</small></span>
                        
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-success" style="width: 40%"></div>
                            </div>
                            <span>warning</span><span class="pull-right"><small>60%</small></span>
                        
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
                            </div>
                            <span>Danger</span><span class="pull-right"><small>80%</small></span>
                        
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-danger" style="width: 80%"></div>
                            </div>
                        </div>

                    <!-- /#right -->

            </div>


                  
@endsection

@push('scripts')
<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
  $(function(){
    $("#personal, #business").change(function(){

        $("#race, #religion, #occupation, #marital, #salutation, #gender, #employement_status", "#nationality").val("").attr("disabled",true);
        if($("#personal").is(":checked")){
            $("#race").removeAttr("disabled");
            $("#race").focus();
            $("#religion").removeAttr("disabled");
            $("#religion").focus();
             $("#salutation").removeAttr("disabled");
            $("#salutation").focus();
             $("#marital").removeAttr("disabled");
            $("#marital").focus();
             $("#occupation").removeAttr("disabled");
            $("#occupation").focus();
            $("#gender").removeAttr("disabled");
            $("#gender").focus();
             $("#nationality").removeAttr("disabled");
            $("#nationality").focus();
             $("#financial_type").removeAttr("disabled");
            $("#financial_type").focus();
            $("#employement_status").removeAttr("disabled");
            $("#employement_status").focus();
        }
       
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>

@endpush