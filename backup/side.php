<?php
header('X-Frame-Options: SAMEORIGIN');
?>
<html>
	<head>
	<style type="text/css">
	body {
		font-size:13px;
	}
	td.besar {
		
	}
	td.header {
		background-color: #e10606;
		font-weight:bold; 
	}
  	td.border {
	 	border-collapse: collapse;
		border: 1px solid black;
    }
 	td.foroffice {


	  	padding: 8px; /* cellpadding */
   		line-height: 130%;
 
    }
   
     td.besar:first-letter {text-transform: uppercase;}

    table.border {
	  border-collapse: collapse;
	    border: 1px solid black;
	}


#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
.isResizable {
  background: rgba(255, 0, 0, 0.2);
  font-size: 2em;
  border: 1px solid black;
  overflow: hidden;
  resize: both;
  width: 160px;
  height: 120px;
  min-width: 120px;
  min-height: 90px;
  max-width: 400px;
  max-height: 300px;
}

	</style>
	<style>
.scroll{
  width: 90%;
  padding: 10px;
  overflow: scroll;
  height: 1000px;
  
  /*script tambahan khusus untuk IE */
  scrollbar-face-color: #CE7E00; 
  scrollbar-shadow-color: #FFFFFF; 
  scrollbar-highlight-color: #6F4709; 
  scrollbar-3dlight-color: #11111; 
  scrollbar-darkshadow-color: #6F4709; 
  scrollbar-track-color: #FFE8C1; 
  scrollbar-arrow-color: #6F4709;
}
		
.column {
    float: left;
    width: 50%;
}

.column2 {
    float: left;
    width: 50%;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}	
.column {
    float: left;
}

.left {
    width: 25%;
}

.right {
    width: 75%;
}
@media (max-width: 600px) {
    .column {
        width: 100%;
    }
}
</style>
	</head>
	<body>
		
         
<div class="row">
  <div class="column">
  	<div class="scroll">
  		<table width="100%" >
			<tbody>
				
				<tr>
					<td colspan="2">&nbsp;<h2>ZURICH TAKAFUL INSURANCE APPLICATION FORM</h2></td>
				</tr>
				
			</tbody>
		</table>

		<table width="100%" cellspacing="4" id="customers">
			
			<tbody>
				<tr>
					<td colspan="7" class="header">DETAILS</td>
				</tr>
				<tr>
					<td width="40%">Transaction Type</td>
					<td width="5%">:</td>
					<td width="55%" class="underline">
						@if($vehicle->transactiontype->label=='Registered')
						New Business / Renewal
						@elseif($vehicle->transactiontype->label=='New Registration')
						New Vehicle Registration
						@else
						Transfer Ownership
						@endif
					</td>
				</tr>
				<tr>
					<td width="40%">Vehicle Reg Number</td>
					<td width="5%">:</td>
					<td width="55%" class="underline">{{$vehicle->reg_vehicle_number}}</td>
				</tr>
				<tr>
					<td colspan="7" class="header">COVERAGE DETAILS</td>
				</tr>
				<tr>
					<td>Product Code</td>
					<td>:</td>
					<td class="besar">{{$vehicle->covernoteclass->label}}</td>
				</tr>
				<tr>
					<td>Coverage Type</td>
					<td>:</td>
					<td class="besar">{{$vehicle->coverage_type}}</td>
				</tr>
				<tr>
					<td>Period of Cover</td>
					<td>:</td>
					<td class="besar">From : <b>{{$vehicle->period_of_cover_start}}</b> To: <b>{{$vehicle->period_of_cover_end}}</b></td>
				</tr>

				<tr>
					<td colspan="7" class="header">VEHICLE DETAILS</td>
				</tr>
				<tr>
					<td>Year of Make</td>
					<td>:</td>
					<td class="underline">{{$vehicle->seriess->year}}</td>
				</tr>
				<tr>
					<td>Make</td>
					<td>:</td>
					<td class="underline">{{$vehicle->brand->brand}}</td>
				</tr>
				<tr>
					<td>Model</td>
					<td>:</td>
					<td class="underline">{{$vehicle->model->model_name}}</td>
				</tr>

				<tr>
					<td>Engine Capacity</td>
					<td>:</td>
					<td class="underline">{{$vehicle->seriess->cc}}</td>
				</tr>
				<tr>
					<td>Variant Series</td>
					<td>:</td>
					<td class="underline">{{$vehicle->seriess->series}} {{$vehicle->seriess->transmission}}</td>
				</tr>
				<tr>
					<td>Region Code / Place of used</td>
					<td>:</td>
					<td class="underline">
						@if($vehicle->location_vehicle_use==1)
						Peninsular (West Malaysia)
						@else
						Sabar / Sarawak (East Malaysia)
						@endif
					</td>
				</tr>
				<tr>
					<td>Number of Passengers</td>
					<td>:</td>
					<td class="underline">{{$vehicle->seriess->seat}}</td>
				</tr>
				@if($vehicle->modified==1)
				<tr>
					<td>Is modified Car?</td>
					<td>:</td>
					<td class="underline">Yes</td>
				</tr>
				<tr>
					<td>Performance Based Modification Type</td>
					<td>:</td>
					<td class="underline">{{$vehicle->performance->label}}</td>
				</tr>
				<tr>
					<td>Functional Based Modification Type</td>
					<td>:</td>
					<td class="underline">{{$vehicle->performance->functional}}</td>
				</tr>
				@elseif($vehicle->modified==0)
				<tr>
					<td>Is modified Car?</td>
					<td>:</td>
					<td class="underline">No</td>
				</tr>
				@endif

			</tbody>
		</table>
		<table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
			<tbody>
				<tr>
					<td class="header" colspan="7">PARTICIPANT DETAILS</td>
				</tr>
				<tr>
					<td width="40%">Participant Indicator <sub>*</sub></td>
					<td width="5%">:</td>
					<td width="55%" class="besar">{{$applicant->purpose_insurance}}</td>
				</tr>
				<tr>
					<td width="40%">Name <sub>*</sub></td>
					<td width="5%">:</td>
					<td width="55%" class="besar">{{$applicant->fullname}}</td>
				</tr>

			@if($applicant->purpose_insurance =='personal')
				<?php
					$ic = $applicant->ic_number;
					if(ctype_digit($ic) && strlen($ic) == 12) {
 					$ic = substr($ic, 0, 6) .'-'.
            		substr($ic, 6, 2) .'-'.
            		substr($ic, 8);
					}
				?>
				<tr>
					<td>New IC Number <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$ic}}</td>
				</tr>
				<tr>
					<td>Gender <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$applicant->Gender->label}}</td>
				</tr>
				<tr>
					<td>Marital Status <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$applicant->Marital->label}}</td>
				</tr>
				<tr>
					<td>Occupation <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$applicant->Job->label}}</td>
				</tr>
				<tr>
					<td>Mobile Number <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$applicant->phone}}</td>
				</tr>
				<tr>
					<td>Email Address</td>
					<td>:</td>
					<td>{{$applicant->email}}</td>
				</tr>
				<tr>
					<td>Address <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$address->address_1}}</td>
				</tr>
				<tr>
					<td>Postcode <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$address->postcode_1}}</td>
				</tr>
				<tr>
					<td>Town <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$address->city_1}}</td>
				</tr>
				<tr>
					<td>State <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$address->state_1}}</td>
				</tr>
				<tr>
					<td>Country <sub>*</sub></td>
					<td>:</td>
					<td class="besar">{{$address->country_1}}</td>
				</tr>
			@endif
			</tbody>
		</table>

		<table width="100%" cellspacing="4" style="page-break-before: always;" id="customers">
			<tbody>
				<tr>
					<td class="header" colspan="7">PARTICULARS OF ADDONS INSURANCE</td>
				</tr>
				<tr>
					<td width="20%"><b>Extra Coverage</b></td>
					<td width="5%"></td>
               		<td width="5%"><b>Option</b></td>
               		<td width="5%"></td>
               		<td width="20%"><b>Amount</b></td>
               		<td width="20%"><b>Sum Insured</b></td>
               		<td width="15%"><b>Premium</b></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
               		<td></td>
               		<td></td>
               		<td></td>
               		<td></td>
               		<td></td>
				</tr>

				@if($purchase_addon->legal_liability_to_psg_ck =='1')
				<tr>
					<td>Legal Liability to Passengers</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
				</tr>
				@endif

				@if($purchase_addon->cart_ck =='1')
				<tr>
					<td>Compensation for Assessed Repair Time (CART)</td>
					<td>:</td>
               		<td>YES</td>
               		<td><b>{{$purchase_addon->cart_day}} Day(s)</b></td>
               		<td><b>{{$purchase_addon->cart_amount}} Day(s)</b></td>
               		<td><b>RM {{$purchase_addon->cart_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->cart_insured}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->pa_basic_ck =='1')
				<tr>
					<td>Private PA Basic</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td><b>{{$purchase_addon->pa_basic_unit}} Unit(s)</b></td>
               		<td><b>RM {{$purchase_addon->pa_basic_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->pa_basic_insured}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->temporary_ck =='1')
				<tr>
					<td>Temporary Courtesy Car</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td><b>{{$purchase_addon->temporary_day}} Day(s)</b></td>
               		<td><b>RM {{$purchase_addon->temporary_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->temporary_insured}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->towing_ck =='1')
				<tr>
					<td>Towing & Cleaning due to water damage</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td><b>{{$purchase_addon->towing_amount}}</b></td>
               		<td><b>RM {{$purchase_addon->towing_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->towing_insured}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->key_ck =='1')
				<tr>
					<td>Key Replacement</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td><b>{{$purchase_addon->key_amount}}</b></td>
               		<td><b>RM {{$purchase_addon->key_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->key_insured}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->caravan_ck =='1')
				<tr>
					<td>Caravan/Luggage (Private Car Policy Only)</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->caravan_suminsured}}</b></td>
               		<td>&nbsp;</td>
				</tr>
				@endif

				@if($purchase_addon->strike_ck =='1')
				<tr>
					<td>Strike, Riot and Civil Commotion</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
				</tr>
				@endif

				@if($purchase_addon->perils_flood_ck =='1')
				<tr>
					<td>Inclusion of Special Perils</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
				</tr>
				@endif

				@if($purchase_addon->legal_liability_of_psg_ck =='1')
				<tr>
					<td>L.L.O.P For Acts of Negligence</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->legal_liability_of_psg}}</b></td>
				</tr>
				@endif

				@if($purchase_addon->windscreen_ck =='1')
				<tr>
					<td>Windscreen Damage</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->windscreen_suminsured}}</b></td>
               		<td>&nbsp;</td>
				</tr>
				@endif

				@if($purchase_addon->veh_accessories_ck =='1')
				<tr>
					<td>Vehicle Accessories Endorsment</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->veh_accessories_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->veh_accessories_insured}}</b></td>
				</tr>
				@endif
				@if($purchase_addon->ncd_relief_ck =='1')
				<tr>
					<td>NCD Relief</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
				</tr>
				@endif
				@if($purchase_addon->ngv_ck =='1')
				<tr>
					<td>Gas Conversion Kit And Tank</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->ngv_suminsured}}</b></td>
               		<td><b>RM {{$purchase_addon->ngv_insured}}</b></td>
				</tr>
				@endif

				

				<tr>
					<td colspan="3">Drivers Addon</td>
				</tr>
				<tr>
					<td>&nbsp; &nbsp; a) Driver Addon</td>
					<td>:</td>
               		<td>YES</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>&nbsp; &nbsp; b) Number of Drivers</td>
					<td>:</td>
					<td class="besar">{{$purchase_addon->driver_amount}} person</td>
					<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td>&nbsp;</td>
               		<td><b>RM {{$purchase_addon->driver_insured}}</b</td>
				</tr>
				
			</tbody>
		</table>

		 @if($purchase_addon->driver_ck =='1')
		<table width="100%" cellspacing="7" id="customers">
			<tbody>
				<tr>
					<td class="header" colspan="4">NAME OF DRIVERS</td>
				</tr>
			</tbody>
		</table>

		<table width="98%" cellspacing="4" align="center"  style="page-break-before: always;" id="customers">
			<tbody>

				<tr>
					<td class="border">Name of Driver</td>
					<td class="border">IC Number</td>
					<td class="border">Marital</td>
					<td class="border">Occupation</td>
					<td class="border">Relationship</td>
					<td class="border">Main Driver</td>
				</tr>
				@foreach($driver as $pur)
				<tr>
					<td class="border">{{$pur->fullname}}</td>
					<td class="border">{{$pur->ic_number}}</td>
					@if($pur->marital_status!=NULL)
						<td class="border">{{$pur->MaritalStatus->label}}</td>
					@else
					<td class="border">POLICYHOLDER</td>
					@endif
					@if($pur->occupation!=NULL)
						<td class="border">{{$pur->Occupation->label}}</td>
					@else
					<td class="border">POLICYHOLDER</td>
					@endif
					@if($pur->relationship!='0')
						<td class="border">{{$pur->Relation->label}}</td>
					@else
					<td class="border">POLICYHOLDER</td>
					@endif
					@if($pur->is_main_driver=='1')
					<td class="border">O</td>
					@else
					<td class="border">X</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
		<table width="100%" cellspacing="4">
			<tbody>
				<tr>
					<td class="header" colspan="4">NAME OF DRIVERS</td>
				</tr>
			</tbody>
		</table>

		<table width="98%" cellspacing="4" align="center" class="border">
			<tbody>

				<tr>
					<td class="border">Name of Driver</td>
					<td class="border">IC Number</td>
				</tr>
				@foreach($driver as $pur)
				<tr>
					<td class="border">{{$pur->fullname}}</td>
					<td class="border">{{$pur->ic_number}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</b></td></tr></tbody></table></div>
  	</div>
  <!--<div class="column2">
  	<div class="scroll">
  		<iframe src="https://egms.zurich.com.my/eMotor/CoverNote/NewQuotation.aspx" border="0" framspacing="0" marginheight="0" marginwidth="0" vspace="0" hspace="0" frameborder="0" height="100%"  width="100%"></iframe>

  		

    </div>
  </div>-->
  

</body>
</html>

<script language="JavaScript" type="text/javascript">
function closeSelf(){
    // do something

    var win_timer = setInterval(function() {   
   
          window.close();
          clearInterval(win_timer);
      
      }, 300); 
}

</script>
<script type="text/javascript">$('.capitalize').each(function(){
    var text = this.innerText;
    var words = text.split(" ");
    var spans = [];
    var _this = $(this);
    this.innerHTML = "";
    words.forEach(function(word, index){
        _this.append($('<span>', {text: word}));
    });
});</script>