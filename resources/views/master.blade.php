@extends('layouts.master')

@section('container')    
	
    @include('layouts.menu')
    @yield('menu')

	@include('layouts.head')
    @yield('head')

    <div class="content">
    	@yield('content')
    </div>

    @include('layouts.footer')
    @yield('footer')


@stop

  