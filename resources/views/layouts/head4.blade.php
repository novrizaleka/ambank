@section('head4')
<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Netxpert Sdn Bhd</title>
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="{{ asset('assets/public/assets/img/metis-tile.png') }}" />

    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/bootstrap/css/bootstrap.css') }}">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href=" {{ asset('assets/public/assets/lib/font-awesome/css/font-awesome.css') }}">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/css/main.css') }}">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/metismenu/metisMenu.css') }}">
    
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/onoffcanvas/onoffcanvas.css') }}">
    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/animate.css/animate.css') }}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css">

        <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/jquery.gritter/css/jquery.gritter.css') }}">
   <link rel="stylesheet" href=" {{ asset('assets/public/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css') }}">

          <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/inputlimiter/jquery.inputlimiter.css') }}">
          
        <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/bootstrap-daterangepicker/daterangepicker-bs3.css') }}">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.3/jquery.tagsinput.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker3.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.0.1/css/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
          <!-- Css Custom -->
        <link rel="stylesheet" href="{{ asset('assets\public\assets\css\custom.css') }}">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!--For Development Only. Not required -->
    <script>
        less = {
            env: "development",
            relativeUrls: false,
            rootpath: "/assets/"
        };
    </script>
    <!--<link rel="stylesheet" href="{{ asset('assets/public/assets/css/style-switcher.css') }}">-->
    <link rel="stylesheet/less" type="text/css" href="{{ asset('assets/public/assets/less/theme.less') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>

  </head>
   <body class="  ">
            <div class="bg-dark dk" id="wrap">
                <div id="top">
                    <!-- .navbar -->
                
                    <!-- /.navbar -->
                       <!-- <header class="head">
                                <div class="search-bar">
                                    <form class="main-search" action="">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Live Search ...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary btn-sm text-muted" type="button">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-home"></i>&nbsp;
            NetXpert Sdn Bhd
          </h3>
                            </div>
                        </header>-->
                </div>
                <!-- /#top -->
                    
                        
                    </div>
   @endsection