
<!DOCTYPE html>
<html lang="en">

@include('layouts.head4')
@yield('head4')

<body class="menu-on-top" data-spy="scroll">

    <div id="wrapper">
        @yield('container')    
    </div>
   
  

  
@stack('scripts')
</body>
</html>
