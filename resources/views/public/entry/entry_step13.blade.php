@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row pink">
                <div class="col-lg-12">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                           <form class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/save_step8')}}">
                            {{csrf_field()}}
                            <input type="text" name="account" value="">
                            <fieldset class="step" id="first">
                         <h4 class="text-danger pull-right"> Current Employment</h4>
                        <div class="clearfix"></div>
                        
                        <label class=" control-label col-lg-4">Profile Indicator</label>
                        
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_vip">
                              <label for="scales">VIP</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_staff">
                              <label for="scales">Staff</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_info">
                              <label for="scales">Info</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_seraf">
                              <label for="scales">Seraf</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_se_staff">
                              <label for="scales">SE Staff</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_fr">
                              <label for="scales">FR</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_fd_fledge">
                              <label for="scales">FD Pledge</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_doc_ic">
                              <label for="scales">Doc IC</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_doc_ot">
                              <label for="scales">Doc OT</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_bjr">
                              <label for="scales">BJR</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_pin_gen" checked>
                              <label for="scales">Pin Gen</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="si_direct_mailing">
                              <label for="scales">Direct Mailing</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-12">
                              <input type="checkbox" name="si_res_ind">
                              <label for="scales">Res Ind</label>
                            </div>
                          </div>
                        </div>



                        <label class=" control-label col-lg-4 mb-2">Special Interest</label>
                        
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_golf">
                              <label for="scales">Golf</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_travel">
                              <label for="scales">Travel</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_arts_and_culture">
                              <label for="scales">Arts And Culture</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_beuty_and_fashion">
                              <label for="scales">Beuty And Fashion</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_health">
                              <label for="scales">Health</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_dining">
                              <label for="scales">Dining</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_tech_and_gadget">
                              <label for="scales">Tech And Gadget</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_enternaiment">
                              <label for="scales">Enternaiment</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_sport">
                              <label for="scales">Sport</label>
                            </div>
                            <div class="col-xs-3">
                              <input type="checkbox" name="pi_others">
                              <label for="scales">Others</label>
                            </div>
                            <div class="col-xs-3">
                              
                            </div>
                            <div class="col-xs-3">
                              
                            </div>
                            
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="row control-label col-lg-4">
                            <div class="col-xs-12">
                              <input type="checkbox" name="pi_excess_of_card_limits">
                              <label for="scales">Excess Of Card Limit</label>
                            </div>
                            
                            
                          </div>
                        </div>



                      
                          
                            <a href="{{url('/data_entry/step_4')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
   $(function()
    {
        $( "#nature_business" ).autocomplete({
          source: "{{ url('api/nature') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#nature_business').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#occupation" ).autocomplete({
          source: "{{ url('api/occupation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#occupation').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#job_sector" ).autocomplete({
          source: "{{ url('api/job_sector') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#job_sector').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#designation" ).autocomplete({
          source: "{{ url('api/designation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#designation').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#job_sector_prev" ).autocomplete({
          source: "{{ url('api/job_sector') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#job_sector_prev').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#designation_prev" ).autocomplete({
          source: "{{ url('api/designation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#designation_prev_prev').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script type="text/javascript">
     $(function () {

            $('#home_phone_per').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>
<script type="text/javascript">
     $(function () {

            $('#mobile_phone').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>

<script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val(data[k].state.country );
                    });

                }
            });

   
});
</script>
@endpush
