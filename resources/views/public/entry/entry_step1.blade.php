@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="row"  style="background-color: pink">
        <div class="col-lg-12">
            <div class="box dark">
                <div id="div-1" class="body">
                    <form id="step1" method="post" action="{{url('/data_entry/save_step1')}}" class="s-submit clearfix" style="background-color: pink; font-family: Tahoma, Geneva, sans-serif; font-size:8pt">
                    {{csrf_field()}}
                    <input type="text" name="account" value="{{$cc_reg->account_id_no}}">
                    <fieldset class="step" id="first">
                        <h4 class="text-danger pull-right">Choice of Card</h4>
                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">SC Channel / Branch<sup>*</sup></label>
                            </div>
                            <div class="col-xs-2">
                                <input type="text" name="sc_channel" id="sc_channel" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_sc_channel_code}}" tabindex="-1"><span id="results"></span>
                            </div>
                                <div class="col-xs-6">
                                    <!--select name="sc_branchs" id="sc_branchs" class="form-control" required="">
                                        <option value="">Select Branch</option>
                                        @foreach ($sc_branch as $sc_branch)
                                            <option value="{{$sc_branch->source_code_branche_code }}">{{ $sc_branch->source_code_branche_code }}</option>
                                            @endforeach
                                    </select>-->
                                    <input type="text" name="sc_branch" id="sc_branch" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_sc_branch}}" readonly="">
                                </div>
                        </div><br><br>

                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Agent Type / Agent No<sup>*</sup></label>
                            </div>
                            <div class="col-xs-2">
                                <input type="text" name="agent_type" id="agent_type" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_agent_type}}" readonly="">
                            </div>
                            <div class="col-xs-6">
                                 <input type="text" name="agent_no" id="agent_no" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_agent_num}}" readonly="">
                            </div><div class="col-xs-1"></div><br><br>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-4">
                                 <label for="server_name" class="control-label col-lg-4">Introducer's / Agent Name</label>
                            </div>
                            <div class="col-xs-8">
                                <input type="text" name="agent_name" id="agent_name" class="form-control"  value="{{$temp_entry_steps1->tab1_introducer}}" readonly="">
                            </div><div class="col-xs-1"></div><br><br>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="server_name" class="control-label col-lg-4">eCosway Introducer's ID No</label>
                            </div>
                            <div class="col-xs-8">
                                 <input type="text" name="ecosway" id="ecosway" class="form-control" value="{{$temp_entry_steps1->tab1_ecosway}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>

                       <div class="form-group">
                            <div class="col-xs-4">
                                <label for="server_name" class="control-label col-lg-4">Ambank Protector (Y/N)</label>
                            </div>
                            <div class="col-xs-2">
                                  <input type="text" name="ambank_protector" id="ambank_protector" class="form-control" value="{{$temp_entry_steps1->tab1_ambank_protector}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Card Delivery<sup>*</sup></label>
                            </div>
                            <div class="col-xs-8">
                                 <input type="text" name="card_delivery" id="card_delivery" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_card_delivery}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Card Collection<sup>*</sup></label>
                            </div>
                            <div class="col-xs-8">
                                <input type="text" name="card_collection" id="card_collection" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_card_collection}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Collection Branch(State)<sup>*</sup></label>
                            </div>
                            <div class="col-xs-8">
                                <input type="text" name="card_branch_state" id="card_branch_state" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_collection_branch_state}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Collection Branch<sup>*</sup></label>
                            </div>
                            <div class="col-xs-8">
                                <input type="text" name="card_branch" id="card_branch" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps1->tab1_collection_branch}}" readonly="">
                                 <!--<select name="card_branch" id="card_branchs" class="form-control" >
                                    <option></option>
                                </select>-->
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Plastic Data E4</label>
                            </div>
                            <div class="col-xs-8">
                                <input type="text" name="plastic_data" id="plastic_data" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="{{$temp_entry_steps1->tab1_plastic_data}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4">
                                <label for="parameter" class="control-label">Business Owner ID</label>
                            </div>
                            <div class="col-xs-8">
                                 <input type="text" name="business_owner" id="business_owner" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="{{$temp_entry_steps1->tab1_business_owner}}" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-4">
                                 <label for="server_user" class="control-label">Date Applied</label>
                            </div>
                            <div class="col-xs-5">
                                 <input type="date" name="date_applied" id="date_applied" class="form-control"  placeholder="ddmmyy (010118)" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4">
                                 <label for="server_user" class="control-label">AIP Ref No</label>
                            </div>
                            <div class="col-xs-5">
                                  <input type="text" name="aip_refno" id="aip_refno" class="form-control"  placeholder="AIP REF NO" readonly="">
                            </div>
                            <div class="col-xs-1"></div><br><br>
                        </div>
                        

                          <button type="submit" class="btn btn-default pull-right" >Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

 <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $("#step1").validate({
        rules: {
            sc_channel: {
                required: true,
            }
        }
    });
    

});
</script>
<script type="text/javascript">
   $(function()
    {
         $("#sc_channel").focus();
         $( "#sc_channel" ).autocomplete({
          source: "{{ url('api/sc_channel') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#sc_channel').val(ui.item.value);
          }
        });
    });
</script>


<script type="text/javascript">
   $(function()
    {
         $( "#sc_branch" ).autocomplete({
          source: "{{ url('api/sc_branch') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#sc_branch').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#agent_type" ).autocomplete({
          source: "{{ url('api/agenttype') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#agent_type').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#agent_no" ).autocomplete({
          source: "{{ url('api/agentnumber') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#agent_no').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#card_delivery" ).autocomplete({
          source: "{{ url('api/carddelivery') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#card_delivery').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#card_collection" ).autocomplete({
          source: "{{ url('api/cardcollection') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#card_collection').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#card_branch_state" ).autocomplete({
          source: "{{ url('api/cardbranchstate') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#card_branch_state').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#plastic_data" ).autocomplete({
          source: "{{ url('api/plasticdata') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#plastic_data').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#business_owner" ).autocomplete({
          source: "{{ url('api/businessowner') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#business_owner').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#product_type" ).change(function() {
        var product_type = $('#product_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/product_type/"+product_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#program_code").prop('program_code', true); // disable button
                },
                success: function (data, status) {
                    $('#program_code').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#program_code").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].programme_code_desc,
                    }));
                });
                $("#program_code").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applieds').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>

<script type="text/javascript">
   $(document).ready(function() {
     
     $("#sc_channel").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#sc_channel").blur(function() {
        if ($(this).val() != '')
        {
            $("#sc_branch").removeAttr("readonly");  
            $("#agent_type").attr("readonly"); 
            $("#agent_no").attr("readonly");   
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly"); 
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly"); 
            $("#message").html("");      
        }
        else {              
            $("#sc_branch").attr("readonly","readonly");
            $("#agent_type").attr("readonly","readonly");
            $("#agent_no").attr("readonly");  
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly");  
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#sc_branch").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#sc_branch").blur(function() {
        if ($(this).val() != '')
          {
            $("#agent_type").removeAttr("readonly");
            $("#agent_no").attr("readonly");  
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly");
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly"); 
            $("#message").html("");      
          }

        else {              
            $("#agent_type").attr("readonly","readonly"); 
            $("#agent_no").attr("readonly");    
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly");  
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>

<script type="text/javascript">
   $(document).ready(function() {
     
     $("#agent_type").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#agent_type").blur(function() {
        if ($(this).val() != '')
          {
            $("#agent_no").removeAttr("readonly");
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly"); 
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
              $("#message").html("");      
          }
        else {              
            $("#agent_no").attr("readonly","readonly"); 
            $("#agent_name").attr("readonly"); 
            $("#ecosway").attr("readonly");
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");  
            $("#message").append(error_message);      
          }         
      });
 });
</script>

<script type="text/javascript">
   $(document).ready(function() {
     
     $("#agent_no").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#agent_no").blur(function() {
        if ($(this).val() != '')
          {
            $("#agent_name").removeAttr("readonly");
            $("#ecosway").attr("readonly");
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
              $("#message").html("");      
          }
        else {              
            $("#agent_name").attr("readonly","readonly");  
            $("#ecosway").attr("readonly");  
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>

<script type="text/javascript">
   $(document).ready(function() {
     
     $("#agent_name").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#agent_name").blur(function() {
        if ($(this).val() != '')
          {
            $("#ecosway").removeAttr("readonly");
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
              $("#message").html("");      
          }
        else {              
            $("#ecosway").attr("readonly","readonly");   
            $("#ambank_protector").attr("readonly"); 
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#ecosway").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#ecosway").blur(function() {
        if ($(this).val() != '')
          {
            $("#ambank_protector").removeAttr("readonly");
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
              $("#message").html("");      
          }
        else {              
            $("#ambank_protector").attr("readonly","readonly");
            $("#card_delivery").attr("readonly"); 
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#ambank_protector").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#ambank_protector").blur(function() {
        if ($(this).val() != '')
          {
            $("#card_delivery").removeAttr("readonly");
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#card_delivery").attr("readonly","readonly");
            $("#card_collection").attr("readonly"); 
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#card_delivery").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#card_delivery").blur(function() {
        if ($(this).val() != '')
          {
            $("#card_collection").removeAttr("readonly");
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#card_collection").attr("readonly","readonly");
            $("#card_branch_state").attr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#card_collection").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#card_collection").blur(function() {
        if ($(this).val() != '')
          {
            $("#card_branch_state").removeAttr("readonly"); 
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#card_branch_state").attr("readonly","readonly");
            $("#card_branch").attr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#card_branch_state").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#card_branch_state").blur(function() {
        if ($(this).val() != '')
          {
            $("#card_branch").removeAttr("readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#card_branch").attr("readonly","readonly"); 
            $("#plastic_data").attr("readonly"); 
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#card_branch").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#card_branch").blur(function() {
        if ($(this).val() != '')
          {
            $("#plastic_data").removeAttr("readonly"); 
            $("#business_owner").removeAttr("readonly"); 
            $("#date_applied").removeAttr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#plastic_data").attr("readonly","readonly");
            $("#business_owner").attr("readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#plastic_data").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#plastic_data").blur(function() {
        if ($(this).val() != '')
          {
            $("#business_owner").removeAttr("readonly"); 
            $("#date_applied").removeAttr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#business_owner").attr("readonly","readonly"); 
            $("#date_applied").attr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#business_owner").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#business_owner").blur(function() {
        if ($(this).val() != '')
          {
            $("#date_applied").removeAttr("readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").html("");      
          }
        else {              
            $("#date_applied").attr("readonly","readonly"); 
            $("#aip_refno").attr("readonly");
            $("#message").append(error_message);      
          }         
      });
 });
</script>
<script type="text/javascript">
   $(document).ready(function() {
     
     $("#date_applied").focus();
     
     var error_message = "Please fill in SC Channel";
     
      $("#date_applied").blur(function() {
        if ($(this).val() != '')
          {
            $("#aip_refno").removeAttr("readonly"); 
            $("#message").html("");      
          }
        else {              
            $("#aip_refno").attr("readonly","readonly"); 
            $("#message").append(error_message);      
          }         
      });
 });
</script>
@endpush
