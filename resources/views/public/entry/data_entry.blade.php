@extends('public.master3')

@section('content')
<div class="row">
  <div class="col-lg-6">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Dynamic Table</h5>
            </header>
            <div id="collapse4" class="body">
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Account ID & ID No</th>
                        <th>Entry</th>
                    </tr>
                    </thead>
                    <tbody>

                  		<?php $i=1; ?>
                    	@foreach($cc_reg as $cc_reg) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ $cc_reg->account_id_no }}</td>
                            <!--<td><a href="{{url('/')}}/{{$cc_reg->account_id_no}}">Entry</a><a href="{{url('/')}}/entry/{{ substr($cc_reg->account_id_no,10,29) }}">Entry</a></td>-->
                            <td>
                            	<form method="POST"  id="smart-form-register3" class="smart-form client-form" action="{{ url('data_entry/post_entry')}}" >
                                {{ csrf_field() }}
                            	
                            	<input type="hidden" name="account" value="{{ $cc_reg->account_id_no}}">
                               <button type="submit" name="submit" class="btn btn-primary" >

                              ENTRY
                              </button></form>
                            </td>
                            	

                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->
@endsection