@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row">
                <div class="col-lg-12">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                             <input type="hidden" name="account" value="{{$temp_entry->account}}">
                           
                        
                            <form method="post" action=" {{url('/data_entry/save_step10')}}" class="inlineForm" enctype="multipart/form-data" id="popup-validation">
                            {{csrf_field()}}

                        <div class="repeatingSection">
                            <a href="#" class="buttonGray buttonRight deleteFight">Delete</a>
                             <!--<h4 class="text-danger pull-right">Spouse Information</h4>-->
                            <input type="hidden" name="fighter_a_id_1" id="fighter_a_id_1" value="" />
                            <input type="hidden" name="id" value="{{$temp_entry->id}}" value="" />
                            <input type="hidden" name="account" value="{{$temp_entry->account}}" value="" />
                        <div class="form-group">
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">ID Type<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="id_type_spo" id="id_type_spo" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">ID No<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="id_no_spo" id="id_no_spo" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="">
                            </div>
                        </div><br><br>
                         <div class="form-group">
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Alternate ID Type<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="id_type_alt_spo" id="id_type_alt_spo" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Alternate ID No<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="id_no_alt_spo" id="id_no_alt_spo" class="form-control" required="" value="">
                            </div>
                        </div><br><br>
                         <div class="form-group">
                            <label for="Label" class="control-label col-lg-4">Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name" id="name" class="form-control" value="" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Relationship<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="relationship" id="relationship" class="form-control" value="" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Name of Company<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name_company" id="name_company" class="form-control" value="" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                         
                      <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Occupation<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="occupation" id="occupation" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Other Occupation </label>
                            <div class="col-lg-8">
                                <input type="text" name="other_occupation" id="other_occupation" class="form-control" value="" onkeyup="this.value = this.value.toUpperCase()" >
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Home tel.no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="home_phone" id="home_phone" class="form-control" maxlength="12" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-4">Office tel no</label>

                            <div class="col-lg-3">
                                   <input type="text" name="office_phone" id="office_phone" class="form-control" maxlength="12"  value="">
                            </div>
                        </div>
                        </div>
                        <div class="formRowRepeatingSection">
                                <a href="#" class="buttonGray buttonRight addFight">Add Spouse</a>
                            </div>


                          
                         <a href="{{url('/data_entry/step_1')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php for ($x = 1; $x <= 12; $x++) {  ?>
<script type="text/javascript">
   $(function()
    {
        $( "#occupation" ).autocomplete({
          source: "{{ url('api/occupation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#occupation').val(ui.item.value);
          }
        });
        $( "#other_occupation" ).autocomplete({
          source: "{{ url('api/occupation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#other_occupation').val(ui.item.value);
          }
        });
        $( "#relationship" ).autocomplete({
          source: "{{ url('api/relationship') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#relationship').val(ui.item.value);
          }
        });
         $( "#id_type_spo" ).autocomplete({
          source: "{{ url('api/idtype') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#id_type_spo').val(ui.item.value);
          }
        });
         $( "#id_type_alt_spo" ).autocomplete({
          source: "{{ url('api/idtypealt') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#id_type_alt_spo').val(ui.item.value);
          }
        });
    });
</script>

<?php } ?>
<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#product_type" ).change(function() {
        var product_type = $('#product_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/product_type/"+product_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#program_code").prop('program_code', true); // disable button
                },
                success: function (data, status) {
                    $('#program_code').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#program_code").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].programme_code_desc,
                    }));
                });
                $("#program_code").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function goodchars(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;
 
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();
 
// check goodkeys
if (goods.indexOf(keychar) != -1)
    return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
    
if (key == 13) {
    var i;
    for (i = 0; i < field.form.elements.length; i++)
        if (field == field.form.elements[i])
            break;
    i = (i + 1) % field.form.elements.length;
    field.form.elements[i].focus();
    return false;
    };
// else return false
return false;
}
</script>

<script type="text/javascript">
    
    $(document).ready(function(){
    $("#inputTextBox").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){
            event.preventDefault();
        }
    });
});

</script>
<script type="text/javascript">
    // Add a new repeating section
var attrs = ['for', 'id', 'name'];
function resetAttributeNames(section) { 
    var tags = section.find('input, label'), idx = section.index();
    tags.each(function() {
      var $this = $(this);
      $.each(attrs, function(i, attr) {
        var attr_val = $this.attr(attr);
        if (attr_val) {
            $this.attr(attr, attr_val.replace(/_\d+$/, '_'+(idx + 1)))
        }
      })
    })
}
                   
$('.addFight').click(function(e){
        e.preventDefault();
        var lastRepeatingGroup = $('.repeatingSection').last();
        var cloned = lastRepeatingGroup.clone(true)  
        cloned.insertAfter(lastRepeatingGroup);
        resetAttributeNames(cloned)
    });
                    
// Delete a repeating section
$('.deleteFight').click(function(e){
        e.preventDefault();
        var current_fight = $(this).parent('div');
        var other_fights = current_fight.siblings('.repeatingSection');
        if (other_fights.length === 0) {
            alert("You should atleast have one fight");
            return;
        }
        current_fight.slideUp('slow', function() {
            current_fight.remove();
            
            // reset fight indexes
            other_fights.each(function() {
               resetAttributeNames($(this)); 
            })  
            
        })
        
            
    });



</script>
<script type="text/javascript">
    ;(function($){
  "use strict";
  
  Metis.formValidation = function() {
    /*----------- BEGIN validationEngine CODE -------------------------*/
    $('#popup-validation').validationEngine();
    /*----------- END validationEngine CODE -------------------------*/

    /*----------- BEGIN validate CODE -------------------------*/
    $('#inline-validate').validate({
        rules: {
            required: "required",
            id_no_spo: {
                required: true
                
            },
            date: {
                required: true,
                date: true
            },
            url: {
                required: true,
                url: true
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            agree: "required",
            minsize: {
                required: true,
                minlength: 3
            },
            maxsize: {
                required: true,
                maxlength: 6
            },
            minNum: {
                required: true,
                min: 3
            },
            maxNum: {
                required: true,
                max: 16
            }
        },
        errorClass: 'help-block col-lg-6',
        errorElement: 'span',
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
</script>
@endpush
