@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row" style="background-color: pink">
                <div class="col-lg-12">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                           <form style="background-color: pink;" class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/save_step7')}}">
                            {{csrf_field()}}
                            <input type="text" name="account" value="{{$temp_entry_steps7->account}}">
                            <fieldset class="step" id="first">
                         <h4 class="text-danger pull-right"> Current Employment</h4>
                        <div class="clearfix"></div>
                        
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Employment Type<sup>*</sup></label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="employement_type" id="employement_type" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps7->temp_tab7_emp_type}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label col-lg-4">Employer Name<sup>*</sup></label>
                          </div>
                          <div class="col-xs-10">
                                <input type="text" name="employer_name" id="employer_name" class="form-control" value="{{$temp_entry_steps7->temp_tab7_empr_name}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="sc_channel" class="control-label col-lg-4">Department</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="department" id="department" class="form-control" placeholder="Department" value="{{$temp_entry_steps7->temp_tab7_department}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label col-lg-4">Office Address</label>
                          </div>
                          <div class="col-xs-10">
                              <input type="text" name="address_1" id="address_1" class="form-control" value="{{$temp_entry_steps7->temp_tab7_address_1}}">
                          </div>
                          <div class="col-xs-2">
                             <label for="table_collation" class="control-label col-lg-4"></label>
                            </div>
                            <div class="col-xs-10">
                              <input type="text" name="address_2" id="address_2" class="form-control" value="{{$temp_entry_steps7->temp_tab7_address_2}}">
                            </div>
                            <div class="col-xs-2">
                              <label for="table_collation" class="control-label col-lg-4"></label>
                            </div>
                             <div class="col-xs-10">
                                   <input type="text" name="address_3" id="address_3" class="form-control" value="{{$temp_entry_steps7->temp_tab7_address_3}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="sc_channel" class="control-label col-lg-4">Postcode<sup>*</sup></label>
                          </div>
                          <div class="col-xs-4">
                                <input type="text" name="postcode" id="postcode" class="form-control" placeholder="Postcode" value="{{$temp_entry_steps7->temp_tab7_postcode}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label col-lg-4" >City<sup>*</sup></label>
                             </div>
                             <div class="col-xs-4">
                                 <input type="text" name="city" id="city" class="form-control" value="{{$temp_entry_steps7->temp_tab7_city}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">State</label>
                          </div>
                          <div class="col-xs-4">
                                   <input type="text" name="state" id="state" class="form-control" value="{{$temp_entry_steps7->temp_tab7_state}}" readonly="">
                            </div>
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Country</label>
                          </div>
                            <div class="col-xs-4">
                                <input type="text" name="country" id="country" class="form-control" value="{{$temp_entry_steps7->temp_tab7_country}}" readonly="">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label col-lg-4">Office Tel. No</label>
                          </div>
                          <div class="col-xs-10">
                            <input type="text" name="office_phone" id="office_phone" class="form-control" maxlength="12" value="{{$temp_entry_steps7->temp_tab7_office_phone}}">
                          </div>
                          
                        </div>
                        

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-labels">Fax No</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="fax_num" id="fax_num" class="form-control" maxlength="12"  value="{{$temp_entry_steps7->temp_tab7_fax_num}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">Email</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="email" name="email" id="email" class="form-control" value="{{$temp_entry_steps7->temp_tab7_email}}">
                            </div>
                        </div>
                    
                            <a href="{{url('/data_entry/step_4')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
   $(function()
    {
         $( "#employement_type" ).autocomplete({
          source: "{{ url('api/emp_type') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#employement_type').val(ui.item.value);
          }
        });
    });
</script>



<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script type="text/javascript">
     $(function () {

            $('#home_phone_per').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>
<script type="text/javascript">
     $(function () {

            $('#mobile_phone').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>

<script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val(data[k].state.country );
                    });

                }
            });

   
});
</script>
@endpush
