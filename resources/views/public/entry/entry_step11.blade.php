@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row">
                <div class="col-lg-12">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                           <form class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/save_step4')}}">
                            {{csrf_field()}}
                            <input type="text" name="account" value="{{$temp_entry->account}}">
                            <fieldset class="step" id="first">
                         <h4 class="text-danger pull-right">Individual CIF Information</h4>
                        <div class="clearfix"></div>
                         <div class="form-group">
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">ID Type<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="id_type" id="id_type" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_id_type}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">ID No<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="id_no" id="id_no" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_id_no}}">
                            </div>
                        </div>
                        <br><br>
                         <div class="form-group">
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Alternate ID Type<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="id_alt" id="id_alt" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="{{$temp_entry->temp_tab4_alternate_id_type}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Alternate ID No<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="id_no_alt" id="id_no_alt" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="{{$temp_entry->temp_tab4_alternate_id_no}}">
                            </div>
                        </div><br><br>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Customer Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="customer_name" id="customer_name" class="form-control" value="{{$temp_entry->temp_tab4_customer_name}}" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-2">
                                 <label for="parameter" class="control-label col-lg-4">Salutation<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="salutation" id="salutation" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_salutation}}" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Title</label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="title" id="title" class="form-control" onkeyup="this.value = this.value.toUpperCase()"  value="{{$temp_entry->tab1_title}}">
                            </div>
                        </div>
                        <br><br>
                         <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Name On Card<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="name_on_card" id="name_on_card" class="form-control" value="{{$temp_entry->temp_tab4_name_on_card}}" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-2">
                                  <label for="parameter" class="control-label col-lg-4">Sex<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="sex" id="sex" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_sex}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Race<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                               <input type="text" name="race" id="race" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_race}}">
                            </div>
                        </div>
                        <br><br>
                       <div class="form-group">
                            <div class="col-xs-2">
                                 <label for="parameter" class="control-label col-lg-4">Religion<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="religion" id="religion" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_religion}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Ethnic<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                               <input type="text" name="ethnic" id="ethnic" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_ethnic}}">
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <div class="col-xs-2">
                                 <label for="parameter" class="control-label col-lg-4">Nationality<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="nationality" id="nationality" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_nationality}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Citizenship<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="citizenship" id="citizenship" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_citizenship}}">
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <div class="col-xs-2">
                                 <label for="label" class="control-label col-lg-4">DOB<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="text" name="dob" id="dob" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_dob}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">No of Dependants<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="no_dependants" id="no_dependants" class="form-control"  onKeyPress="return goodchars(event,'1234567890',this)" value="{{$temp_entry->temp_tab4_num_dependants}}">
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <div class="col-xs-2">
                                 <label for="parameter" class="control-label col-lg-4">Marital Status<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                  <input type="text" name="marital" id="marital" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_marital_status}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="parameter" class="control-label">Education Level<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="education" id="education" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_edu_level}}">
                            </div>
                        </div>
                        <br><br>
                        
                        <div class="form-group">
                            <label for="parameter" class="control-label col-lg-4">Ambank Staff<sup>*</sup></label>
                            <div class="col-lg-3">
                                <input type="text" name="ambank_staff" id="ambank_staff" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_ambank_staff}}">
                            </div>
                             <div class="col-lg-2">
                                <label for="parameter">VIP Customer<sup>*</sup></label>
                             </div>
                             <div class="col-lg-3">
                                <input type="text" name="vip" id="vip" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry->temp_tab4_vip}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="server_name" class="control-label col-lg-4">Mother's Name<sup>*</sup></label>
                            <div class="col-lg-8">
                                <input type="text" name="mother_name" id="mother_name" class="form-control" value="{{$temp_entry->temp_tab4_mother_name}}" onkeyup="this.value = this.value.toUpperCase()" onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)" >
                            </div>
                        </div>
                         <a href="{{url('/data_entry/step_1')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
   $(function()
    {
         $( "#id_type" ).autocomplete({
          source: "{{ url('api/idtype') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#id_type').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#id_alt" ).autocomplete({
          source: "{{ url('api/idtypealt') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#id_alt').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#salutation" ).autocomplete({
          source: "{{ url('api/salutation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#salutation').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#sex" ).autocomplete({
          source: "{{ url('api/sex') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#sex').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#race" ).autocomplete({
          source: "{{ url('api/race') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#race').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#religion" ).autocomplete({
          source: "{{ url('api/religion') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#religion').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#ethnic" ).autocomplete({
          source: "{{ url('api/ethnic') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#ethnic').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#card_branch_state" ).autocomplete({
          source: "{{ url('api/cardbranchstate') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#card_branch_state').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#nationality" ).autocomplete({
          source: "{{ url('api/nationality') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#nationality').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#title" ).autocomplete({
          source: "{{ url('api/title') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#title').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#citizenship" ).autocomplete({
          source: "{{ url('api/citizenship') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#citizenship').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#marital" ).autocomplete({
          source: "{{ url('api/marital') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#marital').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#education" ).autocomplete({
          source: "{{ url('api/education') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#education').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#ambank_staff" ).autocomplete({
          source: "{{ url('api/ambank_staff') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#ambank_staff').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#vip" ).autocomplete({
          source: "{{ url('api/vip') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#vip').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#product_type" ).change(function() {
        var product_type = $('#product_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/product_type/"+product_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#program_code").prop('program_code', true); // disable button
                },
                success: function (data, status) {
                    $('#program_code').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#program_code").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].programme_code_desc,
                    }));
                });
                $("#program_code").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function goodchars(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;
 
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();
 
// check goodkeys
if (goods.indexOf(keychar) != -1)
    return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
    
if (key == 13) {
    var i;
    for (i = 0; i < field.form.elements.length; i++)
        if (field == field.form.elements[i])
            break;
    i = (i + 1) % field.form.elements.length;
    field.form.elements[i].focus();
    return false;
    };
// else return false
return false;
}
</script>

<script type="text/javascript">
    
    $(document).ready(function(){
    $("#inputTextBox").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){
            event.preventDefault();
        }
    });
});

</script>

@endpush
