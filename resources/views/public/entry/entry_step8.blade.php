@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row pink">
                <div class="col-lg-12">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                           <form class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/save_step8')}}">
                            {{csrf_field()}}
                            <input type="text" name="account" value="{{$temp_entry_steps8->account}}">
                            <fieldset class="step" id="first">
                         <h4 class="text-danger pull-right"> Current Employment</h4>
                        <div class="clearfix"></div>
                        
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Nature of Business<sup>*</sup></label>
                          </div>
                          <div class="col-xs-10">
                                <input type="text" name="nature_business" id="nature_business" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_nature_business}}">
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Occupation<sup>*</sup></label>
                          </div>
                          <div class="col-xs-10">
                                <input type="text" name="occupation" id="occupation" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_occupation}}">
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label col-lg-4">Occupation Description</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="occupation_desc" id="occupation_desc" class="form-control" value="{{$temp_entry_steps8->temp_tab8_occupation_desc}}" onkeyup="this.value = this.value.toUpperCase()" >
                            </div>
                        </div>

                         <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Job Sector<sup>*</sup></label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="job_sector" id="job_sector" class="form-control" value="{{$temp_entry_steps8->temp_tab8_job_sector}}" onkeyup="this.value = this.value.toUpperCase()">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Designation</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="designation" id="designation" class="form-control" value="{{$temp_entry_steps8->temp_tab8_designation}}" onkeyup="this.value = this.value.toUpperCase()" >
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-2">
                                <label for="label" class="control-label"> Joined <sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="date" name="date_joined" id="date_joined" class="form-control" value="{{$temp_entry_steps8->temp_tab8_date_joined}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Year<sup>*</sup></label>
                            </div>
                            <div class="col-xs-2">
                                 <input type="text" name="year_emp" id="year_emp" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_year_emp}}">
                            </div>
                             <div class="col-xs-2">
                                <input type="text" name="month_emp" id="month_emp" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_month_emp}}">
                            </div>
                        </div>
                        <br><br>
                         <div class="form-group">
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Monthly Income+<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="monthly_income" id="monthly_income" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_monthly_income}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Annual Income+<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="annual_income" id="annual_income" class="form-control" required="" value="{{$temp_entry_steps8->temp_tab8_annual_income}}">
                            </div>
                        </div><br><br>
                        
                        
                          <div class="form-group">
                            <label for="table_collation" class="control-label col-lg-12">PREVIOUS EMPLOYEMENT<sup>*</sup></label>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label col-lg-4">Name of Company<sup>*</sup></label>
                          </div>
                            <div class="col-xs-10">
                                   <input type="text" name="name_company_prev" id="name_company_prev" class="form-control" value="{{$temp_entry_steps8->temp_tab8_prev_company}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Job Sector<sup>*</sup></label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="job_sector_prev" id="job_sector_prev" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_prev_job_sector}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label col-lg-4">Designation</label>
                          </div>
                            <div class="col-xs-10">
                                <input type="text" name="designation_prev" id="designation_prev" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_prev_designation}}">
                            </div>
                        </div>

                       <div class="form-group">
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Date Joined<sup>*</sup></label>
                            </div>
                            <div class="col-xs-4">
                                 <input type="date" name="date_joined_prev" id="date_joined_prev" class="form-control" value="{{$temp_entry_steps8->temp_tab8_prev_date_joined}}">
                            </div>
                            <div class="col-xs-2">
                                <label for="label" class="control-label">Years of Emp<sup>*</sup></label>
                            </div>
                            <div class="col-xs-2">
                                 <input type="text" name="year_emp_prev" id="year_emp_prev" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_prev_year_emp}}">
                            </div>
                             <div class="col-xs-2">
                                <input type="text" name="month_emp_prev" id="month_emp_prev" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps8->temp_tab8_prev_month_emp}}">
                            </div>
                        </div>
                        <br><br>
                            <a href="{{url('/data_entry/step_4')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
   $(function()
    {
        $( "#nature_business" ).autocomplete({
          source: "{{ url('api/nature') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#nature_business').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#occupation" ).autocomplete({
          source: "{{ url('api/occupation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#occupation').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#job_sector" ).autocomplete({
          source: "{{ url('api/job_sector') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#job_sector').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#designation" ).autocomplete({
          source: "{{ url('api/designation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#designation').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#job_sector_prev" ).autocomplete({
          source: "{{ url('api/job_sector') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#job_sector_prev').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
   $(function()
    {
        $( "#designation_prev" ).autocomplete({
          source: "{{ url('api/designation') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#designation_prev_prev').val(ui.item.value);
          }
        });
    });
</script>
<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script type="text/javascript">
     $(function () {

            $('#home_phone_per').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>
<script type="text/javascript">
     $(function () {

            $('#mobile_phone').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>

<script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val(data[k].state.country );
                    });

                }
            });

   
});
</script>
@endpush
