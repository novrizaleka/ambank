@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
form > * {
    display: block;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row" style="background-color: pink">
                <div class="col-lg-12">
                    <div class="box dark" >
                        <!--<header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body">
                           <form class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/save_step5')}}" style="background-color: pink">
                            {{csrf_field()}}
                             <input type="hidden" name="account" value="{{$cc_reg->account_id_no}}">
                            <fieldset class="step" id="first">
                         <h4 class="text-danger pull-right">Residence Address</h4>
                        <div class="clearfix"></div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label">Mailing Address<sup>*</sup></label>
                          </div>  
                          <div class="col-xs-10">
                             <input type="text" name="mailing" id="mailing" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps5->temp_tab5_mailing_address}}">
                          </div>
                          <div class="col-xs-1"></div><br><br>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">Home Address</label>
                          </div>
                          <div class="col-xs-10">
                            <input type="text" name="address_1" id="address_1" class="form-control" value="{{$temp_entry_steps5->temp_tab5_address_1}}">
                          </div>
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">&nbsp;</label>
                          </div>
                          <div class="col-xs-10">
                            <input type="text" name="address_2" id="address_2" class="form-control" value="{{$temp_entry_steps5->temp_tab5_address_2}}">
                          </div>
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">&nbsp;</label>
                          </div>
                          <div class="col-xs-10">
                            <input type="text" name="address_3" id="address_3" class="form-control" value="{{$temp_entry_steps5->temp_tab5_address_3}}">
                          </div>
                          
                        </div>

                        <div class="form-group">
                            <div class="col-xs-2">
                                <label for="sc_channel" class="control-label">Postcode<sup>*</sup></label>
                            </div>

                            <div class="col-xs-4">
                                <input type="text" name="postcode" id="postcode" class="form-control" placeholder="Postcode" value="{{$temp_entry_steps5->temp_tab5_postcode}}">
                            </div>
                              <div class="col-xs-2">
                                <label for="parameter" >City<sup>*</sup></label>
                             </div>
                             <div class="col-xs-4">
                                 <input type="text" name="city" id="city" class="form-control" value="{{$temp_entry_steps5->temp_tab5_city}}">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label">State</label>
                          </div>

                          <div class="col-xs-4">
                              <input type="text" name="state" id="state" class="form-control" value="{{$temp_entry_steps5->temp_tab5_state}}" readonly="">
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label">Country</label>
                          </div>

                            <div class="col-xs-4">
                                   <input type="text" name="country" id="country" class="form-control" value="{{$temp_entry_steps5->temp_tab5_country}}" readonly="">
                            </div><br><br>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">Home tel.no</label>
                          </div>
                          <div class="col-xs-10">
                            <input type="text" name="home_phone" id="home_phone" class="form-control" maxlength="12" value="{{$temp_entry_steps5->temp_tab5_home_phone}}">
                          </div>
                          
                        </div>

                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">Mobile phone no</label>
                          </div>
                            <div class="col-xs-10">
                                   <input type="text" name="mobile_phone" id="mobile_phone" class="form-control" maxlength="12"  value="{{$temp_entry_steps5->temp_tab5_mobile_phone}}">
                            </div>
                            <br><br><br>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="table_collation" class="control-label">Email</label>
                          </div>
                            <div class="col-xs-10">
                                   <input type="email" name="email" id="email" class="form-control" value="{{$temp_entry_steps5->temp_tab5_email}}">
                            </div>

                        </div>
                        <div class="form-group">
                          <div class="col-xs-2">
                            <label for="parameter" class="control-label">Residence Type<sup>*</sup></label>
                          </div>
                            <div class="col-xs-10">
                                  <input type="text" name="residence" id="residence" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="" value="{{$temp_entry_steps5->temp_tab5_residence_type}}">
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-xs-2">
                             <label for="table_collation" class="control-label">Years of Stay</label>
                            </div>
                            <div class="col-xs-10">
                               <input type="text" name="year_stay" id="year_stay" class="form-control" placeholder="Year" value="{{$temp_entry_steps5->temp_tab5_year_stay}}">
                            </div>


                              <div class="col-xs-2">
                                <label for="parameter" >Month<sup>*</sup></label>
                             </div>
                             <div class="col-xs-10">
                                 <input type="text" name="month_stay" id="month_stay" class="form-control" placeholder="Month" max="12" value="{{$temp_entry_steps5->temp_tab5_month_stay}}">
                            </div>

                        </div>
                    </fieldset>

                    
                        <br><br>
                            <a href="{{url('/data_entry/step_4')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                          <button type="submit" class="btn m-btn pull-right">Next</button>
                    </fieldset>
                        </div>
                    </div>
                </div>
                
               <!-- <div class="col-lg-6">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="600px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
   $(function()
    {
         $( "#mailing" ).autocomplete({
          source: "{{ url('api/mailing') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#mailing').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
   $(function()
    {
         $( "#residence" ).autocomplete({
          source: "{{ url('api/residence') }}",
          minLength: 1,
          select: function(event, ui) {
            $('#residence').val(ui.item.value);
          }
        });
    });
</script>

<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#product_type" ).change(function() {
        var product_type = $('#product_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/product_type/"+product_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#program_code").prop('program_code', true); // disable button
                },
                success: function (data, status) {
                    $('#program_code').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#program_code").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].programme_code_desc,
                    }));
                });
                $("#program_code").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
        $("input").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $('#aip_refno').blur(function()
{
    $(this).val(function(i, v)
    {
        var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
        return v ? v.join('-') : '';
    });
});
</script>
<script type="text/javascript">
     $(function () {

            $('#home_phone_per').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>
<script type="text/javascript">
     $(function () {

            $('#mobile_phone').keydown(function (e) {
             var key = e.charCode || e.keyCode || 0;
             $text = $(this); 
             if (key !== 8 && key !== 9) {
                 if ($text.val().length === 3) {
                     $text.val($text.val() + '-');
                 }
                 if ($text.val().length === 7) {
                     $text.val($text.val() + '-');
                 }

             }

             return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
         })
});
</script>

<script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val(data[k].state.country );
                    });

                }
            });

   
});
</script>
@endpush
