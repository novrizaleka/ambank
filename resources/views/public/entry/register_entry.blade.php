@extends('public.master3')

@section('content')
<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Dynamic Table</h5>
            </header>
            <div id="collapse4" class="body">
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Account ID & ID No</th>
                        <th>Entry</th>
                        <th>File</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                  		<?php $i=1; ?>
                    	@foreach($files as $file) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ substr($file,10,16) }} - {{ substr($file,27,12) }} -{{ substr($file,10,33) }}</td>
                            <td></td>
                            <td>{{ $file }}</td>
                            <td><a href="{{url('/')}}/{{$file}}">Entry</a><a href="{{url('/')}}/entry/{{ substr($file,10,29) }}">Entry</a></td>
                            <td>
                            	<form method="POST"  id="smart-form-register3" class="smart-form client-form" action="{{ url('post_entry')}}" >
                                {{ csrf_field() }}
                            	
                            	<input type="hidden" name="account" value="{{ substr($file,10,29) }}">
                            	<input type="hidden" name="file" value="{{ substr($file,10,33) }}">
                               <button type="submit" name="submit" class="btn btn-primary">

                              ENTRY
                              </button></form>
                            </td>
                            	

                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->
@endsection