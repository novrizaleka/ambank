@extends('layouts.master')

@section('container')    
	
    

	@include('layouts.head')
    @yield('header')

    <div class="content">
    	@yield('content')
    </div>

    @include('layouts.footer')
    @yield('footer')


@stop

  