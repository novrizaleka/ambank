@extends('public.master3')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Table Bordereds</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
                <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/register/tables')}}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <?php 
                            $today= date('dmy'); // untuk folder name
                        ?>
                     <div class="col-xs-6">
                        <select name="user" id="user" class="form-control" required="">
                            <option selected="">PLEASE SELECT</option>
                            @foreach ($user_log as $user)
                            <option value="{{$user->user_id }}">{{ $user->user_id }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-6">
                        Total: <br>
                        Total: <br>
                        Total :
                    </div> <br><br>
                    <input type="hidden" class="" name="reference_number" id="reference_number" value="REF{{$today}}" >
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <br>
                        <thead>
                            <tr>
                                <th data-checkbox="true" width="5%"></th>
                                <th width="5%">No</th>
                                <th width="45%">Account ID & ID No</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                                @foreach($files as $file) 
                                    <?php 
                                        $cc= substr($file,10,1);

                                         $regis = ($file->getFileInfo());
                                         $da =File::lastModified($file);
                                         $da = DateTime::createFromFormat("U", $da)->setTimezone(new DateTimeZone('America/New_York'));
$da = $da->format('Y-m-d H:i:s');
  $da2 =  date('dmy ', strtotime($da));
                                    ?>
                               
                                    @if($cc =='1')
                                        <tr>
                                            <td>
                                                <input type="checkbox"  name="account_id[]" value="{{ substr($file,10,18) }}" >
                                                <input type="hidden"  name="id[]" value="{{ substr($file,29,12) }}">
                                                <input type="hidden"  name="file[]" value="{{$file->getFileName()}}">
                                                <input type="hidden"  name="date_rcv[]" value="{{$da}}">
                                                <input type="hidden"  name="refnum[]" value="REF{{$da2}}">
                                            </td>
                                            <td>{{$i}} </td>
                                            <td>{{ substr($file,10,18) }} - {{ substr($file,29,12) }}</td>
                                            <td>{{$regis}}</td>
                                            <td>{{$da}}</td>
                                            <!--<td> <a href="JavaScript:newPopup('{{url('/')}}/register/image/{{ substr($file,10,29) }}');" class='btn btn-danger'>Register</a></td>-->
                                        </tr>
                                    @endif
                                <?php  $i++; ?>
                            @endforeach
                        </tbody>  
                    </table>
                    <input type="submit" value="REGISTER" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>

    <div class="col-lg-5">
         <!--<iframe onload="document.getElementById('loadImg').style.display='none';" width="100%" height="400" src="{{url('/')}}/documents/CCA-2018-0062396_850418016087.pdf"></iframe>
           <div class="box">-->
        <div class="box">
            <header>
                <h5>Table Bordereds</h5>
                    <div class="toolbar">
                        <div class="btn-group">
                            <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                                <i class="glyphicon glyphicon-chevron-up"></i>
                            </a>
                            <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
            </header>
            
            <div id="borderedTable" class="body collapse in">
                <table id="dataTable1" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Account ID</th>
                            <th>ID No</th>
                            <th>FEP</th>
                            <th>Entry</th>
                            <th>Remark</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($cc_reg as $cc_reg) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$cc_reg->AccountID}}</td>
                            <td>{{$cc_reg->IDNo}}</td>
                            <td>{{$cc_reg->UserReg}}</td>
                            <td>{{$cc_reg->UserEnt}}</td>
                            <td>
                                @if($cc_reg->status==0)
                                    <span class="label label-warning"  style="font-size: 14px">Pending Entry</span>
                                @elseif($cc_reg->status==1)
                                    <span class="label label-primary"  style="font-size: 14px">Pending Verify</span>
                                @elseif($cc_reg->status==2)
                                    <span class="label label-default"  style="font-size: 14px">Done</span>
                                @elseif($cc_reg->status==9)
                                    <span class="label label-danger"  style="font-size: 14px">Reject</span>
                                @endif
                         </td>
                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>               
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->
<script type="text/javascript">
// Popup window code
function newPopup(url) {
    var win = window.open(
        url,'popUpWindow','height=1000px,width=1350px%,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');

    var win_timer = setInterval(function() {   
      if(win.closed) {
          window.location.reload();
          clearInterval(win_timer);
      } 
      }, 100); 

}
</script>
@endsection