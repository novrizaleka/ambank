<?php
header('X-Frame-Options: SAMEORIGIN');
?>
<html>
<head>
	   <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Metis</title>
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="{{ asset('assets/public/assets/img/metis-tile.png') }}" />
    
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/bootstrap/css/bootstrap.css') }}">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href=" {{ asset('assets/public/assets/lib/font-awesome/css/font-awesome.css') }}">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/css/main.css') }}">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/metismenu/metisMenu.css') }}">
    
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/onoffcanvas/onoffcanvas.css') }}">
    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/animate.css/animate.css') }}">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!--For Development Only. Not required -->
    <script>
        less = {
            env: "development",
            relativeUrls: false,
            rootpath: "/assets/"
        };
    </script>
    <link rel="stylesheet" href="{{ asset('assets/public/assets/css/style-switcher.css') }}">
    <link rel="stylesheet/less" type="text/css" href="{{ asset('assets/public/assets/less/theme.less') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>
	<style type="text/css">
	body {
		font-size:13px;
	}
	td.besar {
		
	}
	td.header {
		background-color: #e10606;
		font-weight:bold; 
	}
  	td.border {
	 	border-collapse: collapse;
		border: 1px solid black;
    }
 	td.foroffice {


	  	padding: 8px; /* cellpadding */
   		line-height: 130%;
 
    }
    td.besar:first-letter {text-transform: uppercase;}

    table.border {
	  border-collapse: collapse;
	    border: 1px solid black;
	}
	#customers {
	    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	    border-collapse: collapse;
	    width: 100%;
	}

	#customers td, #customers th {
	    border: 1px solid #ddd;
	    padding: 8px;
	}

	#customers tr:nth-child(even){background-color: #f2f2f2;}

	#customers tr:hover {background-color: #ddd;}

	#customers th {
	    padding-top: 12px;
	    padding-bottom: 12px;
	    text-align: left;
	    background-color: #4CAF50;
	    color: white;
	}
	</style>
	<style>
	.scroll{
	  width: 90%;
	  padding: 10px;
	  overflow: scroll;
	  height: 1000px;
	  
	  /*script tambahan khusus untuk IE */
	  scrollbar-face-color: #CE7E00; 
	  scrollbar-shadow-color: #FFFFFF; 
	  scrollbar-highlight-color: #6F4709; 
	  scrollbar-3dlight-color: #11111; 
	  scrollbar-darkshadow-color: #6F4709; 
	  scrollbar-track-color: #FFE8C1; 
	  scrollbar-arrow-color: #6F4709;
	}
			
	.column {
	    float: left;
	    width: 50%;
	    padding: 0px;
	    height: 300px; /* Should be removed. Only for demonstration */
	}

	/* Clear floats after the columns */
	.row:after {
	    content: "";
	    display: table;
	    clear: both;
	}	
	.column {
	    float: left;
	}

	.left {
	    width: 25%;
	}

	.right {
	    width: 75%;
	}
	@media (max-width: 600px) {
	    .column {
	        width: 100%;
	    }
	}
	</style>
	<style type="text/css">
	    #content, html, body {
	    height: 98%;
	}
	#left {
	    float: left;
	    width: 50%;
	    height: 1000px;
	    overflow: scroll;
	}
	#right {
	    float: left;
	    width: 50%;
	    background: blue;
	    height: 1000px;
	    overflow: scroll;
	}
	</style>
</head>

<body>
	<div class="row">
  		<div id="left">
  			 <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>Popup Validation</h5>
                <!-- .toolbar -->
            <div class="toolbar">
              <nav style="padding: 8px;">
                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                      <i class="fa fa-minus"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-default btn-xs full-box">
                      <i class="fa fa-expand"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                      <i class="fa fa-times"></i>
                  </a>
              </nav>
            </div>            <!-- /.toolbar -->

            </header>
            <div id="collapse2" class="body">
                <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/register/image')}}" >
                    {{ csrf_field() }}
                	  <?php 
                        $today= date('ymd'); // tanggal hari ini dikurang tanggal selesai
                       
                      ?>
                    <input type="text" class=form-control" name="link" id="link" value="{{$link}}">
                      <input type="hidden" class=form-control" name="file" id="file" value="{{$link}}.pdf">

                    <div class="form-group">
                        <label class="control-label col-lg-4">REFERENCE NO</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" name="reference_number" id="reference_number" value="REF{{$today}}01" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">ACCOUNT NO</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" name="account_no" id="account_no" value="{{ substr($link,0,16) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">ID NO</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" name="id_no" id="id_no" value="{{ substr($link,17,12) }}">
                        </div>
                    </div>
                    @if(!empty($cc_reg->account_id_no))
                    @if(($cc_reg->account_id_no == $link))
                    <div class="form-group">
                        <label class="control-label col-lg-4">CUSTOMER NAME</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" name="customer_name" id="customer_name" onkeyup="this.value = this.value.toUpperCase()" value="{{$cc_reg->CustName}}">
                        </div>
                    </div>
                    
                   @endif
                   @else
                     <div class="form-group">
                        <label class="control-label col-lg-4">CUSTOMER NAME</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" name="customer_name" id="customer_name" onkeyup="this.value = this.value.toUpperCase()" >
                        </div>
                    </div>
                   @endif

                    <div class="form-group">
                        <label class="control-label col-lg-4"></label>
                        <div class="col-lg-4">
                            <button type="button" class="btn btn-default" onclick="closeSelf();">Cancel</button>
                             <input type="submit" value="REGISTER" class="btn btn-primary" onclick="closeSelf();">
                         </div>
                    </div>
                </form>
            </div>
        </div>
  		</div>
		<div id="right">
            <object height="100%"  width="100%" type="application/pdf" data="{{url('/')}}/documents/{{str_replace('/', '', $link)}}.pdf" id="pdf_content">
                <p>Insert your error message here, if the PDF cannot be displayed.</p>
            </object>
         </div>
	</div>
</body>
</html>
	<script src="{{ asset('assets/public/assets/lib/jquery/jquery.js') }}"></script>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>



    <!--Bootstrap -->
    <script src=" {{ asset('assets/public/assets/lib/bootstrap/js/bootstrap.js') }}"></script>
    <!-- MetisMenu -->
    <script src="{{ asset('assets/public/assets/lib/metismenu/metisMenu.js') }}"></script>
    <!-- onoffcanvas -->
    <script src="{{ asset('assets/public/assets/lib/onoffcanvas/onoffcanvas.js') }}"></script>
    <!-- Screenfull -->
    <script src="{{ asset('assets/public/assets/lib/screenfull/screenfull.js') }}"></script>

    <script src=" {{ asset('assets/public/assets/lib/jquery-validation/jquery.validate.js') }}"></script>

            <!-- Metis core scripts -->
            <script src="{{ asset('assets/public/assets/js/core.js') }}"></script>
            <!-- Metis demo scripts -->
            <script src="{{ asset('assets/public/assets/js/app.js') }}"></script>

                <script>
                    $(function() {
                      Metis.formValidation();
                    });
                </script>

            <script src="{{ asset('assets/public/assets/js/style-switcher.js') }}"></script>
<script language="JavaScript" type="text/javascript">
function closeSelf(){
    // do something

    var win_timer = setInterval(function() {   
   
          window.close();
          clearInterval(win_timer);
      
      }, 300); 
}

</script>
<script type="text/javascript">$('.capitalize').each(function(){
    var text = this.innerText;
    var words = text.split(" ");
    var spans = [];
    var _this = $(this);
    this.innerHTML = "";
    words.forEach(function(word, index){
        _this.append($('<span>', {text: word}));
    });
});</script>