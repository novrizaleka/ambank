@extends('public.master3')

@section('content')

<style type="text/css">
	  a.toggler {
    background:  orange;
    cursor: pointer;
    border: 2px solid black;
    border-right-width: 15px;
    padding: 0 5px;
    border-radius: 5px;
    text-decoration: none;
    transition: all .5s ease;
}

a.toggler.off {
    background: red;
    border-right-width: 2px;
    border-left-width: 15px;
}
</style>

	<div class="row">
     <div class="col-lg-2"></div>
     <div class="col-lg-8">
        <div class="box">
            <header>
                <h5>User Data</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <body>
            	<div id="borderedTable" class="body collapse in">
            	<table class="table" id="data-table" class="display nowrap" width="100%">
            		<thead>
            			<tr>
            				<th>User ID</th>
            				<th>Name</th>
            				<th>Status</th>
            				<th class="hidden">Aksi</th>
            			</tr>
            		</thead>
            		<tbody>
                        
                        @foreach($user as $data)
                         <?php
    $todays= date('dmy', strtotime($data->created_at));
    ?>
                        <tr>
            				<td>{{$data->user_id}}</td>
            				<td>{{$data->name}}</td>
            				<td>@if($data->status == 1)
            					<a href="{{url('/userdata/update/'.$data->id)}}" class="toggler" onclick="alert('User akan di nonactif!');">&nbsp;</a>
            					@else
            					<a href="{{url('/userdata/updatea/'.$data->id)}}" class="toggler off" onclick="alert('User akan di Actif!');">&nbsp;</a>
            					@endif
                            </td>
            				<td class="hidden">{{$todays}}</td>
            			</tr>
            			@endforeach
            		</tbody>
            	</table>
            	</div>
            </body>
        </div>
      </div>
      <div class="col-lg-2"></div>
   	</div>


   	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    

    <script type="text/javascript">
    	$(document).ready(function(){
		    $('a.toggler').click(function(){
		        $(this).toggleClass('off');
		    });
		});
    </script>
    <?php
    $today= date('dmy');
    ?>
    <script type="text/javascript">     
        $(document).ready( function () {
          var table = $('#data-table').DataTable({
            "createdRow": function( row, data, dataIndex ) {
                     if ( data[3] != {{$today}} ) {        
                 $(row).css('color', 'Red');
               }
               else {        
                 $(row).css('color', 'Blue');
               }
            }
          });
        } );

    </script>

    

@endsection
