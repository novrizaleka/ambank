@extends('public.master3')

@section('content')
<div class="row">
     <div class="col-lg-5">
          <!--<iframe onload="document.getElementById('loadImg').style.display='none';" width="100%" height="400" src="{{url('/')}}/documents/CCA-2018-0062396_850418016087.pdf"></iframe>-->
           <div class="box">
            <header>
                <h5>Table Bordered</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">

                <table id="dataTables" class="table table-bordered table-condensed table-hover table-striped">
                    
                 <div class="col-xs-6">
                     <select name="user" id="user" class="form-control" required="">
                   
                </select></div>
                 <div class="col-xs-6">total</div><br>           
                </table>
            </div>
        </div>
    </div>
     <div class="col-lg-7">
        <div class="box">
            <header>
                <h5>Table Bordereds</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
 <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/register/tables')}}" >
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <?php 
                        $today= date('dmy'); // tanggal hari ini dikurang tanggal selesai
                       
                      ?>
                         <select name="user" id="user" class="form-control" required="">
                    <option selected="">PLEASE SELECT</option>
                        @foreach ($user as $user)
                            <option value="{{$user->user_id }}">{{ $user->user_id }}</option>
                        @endforeach
                </select>
                 <input type="hidden" class="" name="reference_number" id="reference_number" value="REF{{$today}}" >
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                     
                    <thead>
                        <tr>
                            <th data-checkbox="true" width="5%"></th>
                            <th width="5%">No</th>
                            <th width="45%">Account ID & ID No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($files as $file) 
                            <?php 
                                $cc= substr($file,10,1)
                            ?>
                            @if($cc =='1')
                                <tr>
                                    <td>
                                        <input type="checkbox"  name="account_id[]" value="{{ substr($file,10,18) }}" >
                                        <input type="hidden"  name="id[]" value="{{ substr($file,29,12) }}">
                                        <input type="hidden"  name="file[]" value="{{ substr($file,10,35) }}">
                                    </td>
                                    <td>{{$i}}</td>
                                    <td>{{ substr($file,10,18) }} - {{ substr($file,29,12) }}</td>
                                    <!--<td> <a href="JavaScript:newPopup('{{url('/')}}/register/image/{{ substr($file,10,29) }}');" class='btn btn-danger'>Register</a></td>-->
                                </tr>
                            @endif
                        <?php  $i++; ?>
                        @endforeach

                    </tbody>   

                </table>
                  <input type="submit" value="REGISTER" class="btn btn-primary">
            </div>

            </form>
        </div>
    </div>
   
</div>
<!-- /.row -->
<!--End Datatables-->
<script type="text/javascript">
// Popup window code
function newPopup(url) {
    var win = window.open(
        url,'popUpWindow','height=1000px,width=1350px%,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');

    var win_timer = setInterval(function() {   
      if(win.closed) {
          window.location.reload();
          clearInterval(win_timer);
      } 
      }, 100); 

}
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
  var requiredCheckboxes = $(':checkbox[required]');
  requiredCheckboxes.on('change', function(e) {
    var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr('name') + '"]');
    var isChecked = checkboxGroup.is(':checked');
    checkboxGroup.prop('required', !isChecked);
  });
});
</script>

<script type="text/javascript">
  function displayVals() {
      calcUsage();
      var singleValues = 0; //$("#more").val();         
      $("#span").html("<b>more addons:</b> " + 
                  singleValues + ' EUR<br>');
}
var $cbs = $('.checkbox');
function calcUsage() {
    var total = 0; //$("#more").val();
    $cbs.each(function() {
        if ($(this).is(":checked"))
            total = parseFloat(total) + parseFloat($(this).val());
    });
    $("#usertotal").text(total + ' EUR');
}

    $("select").change(displayVals);
    displayVals();
//For  checkboxes

$cbs.click(function() {
    calcUsage();
});
</script>
<script type="text/javascript">
                  $('#selectAll').click(function(e){
                    var table= $(e.target).closest('table');
                    $('td input:checkbox',table).prop('checked',this.checked);
                  });
                </script>
<script type="text/javascript">
    $(document).ready(function () {
    $("#ckbCheckAll").click(function () {
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
    
    $(".sub_chk").change(function(){
        if (!$(this).prop("checked")){
            $("#ckbCheckAll").prop("checked",false);
        }
    });
});
</script>
@endsection