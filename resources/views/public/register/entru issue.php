@extends('public.master3')

@section('content')
<style type="text/css">
input[type=text] {
    width: 80%;
    padding: 12px 15px;
    margin: 4px 0;
    box-sizing: border-box;
}
input[type=email] {
    width: 80%;
    padding: 12px 15px;
    margin: 4px 0;
    box-sizing: border-box;
}

.center {
    margin-top:50px;   
}

.modal-header {
    padding-bottom: 5px;
}

.modal-footer {
        padding: 0;
    }
    
.modal-footer .btn-group button {
    height:40px;
    border-top-left-radius : 0;
    border-top-right-radius : 0;
    border: none;
    border-right: 1px solid #ddd;
}
    
.modal-footer .btn-group:last-child > button {
    border-right: 0;
}
</style>
<div class="row">
     <div class="col-lg-12">
        <div class="box">
            <header>
                <h5>Table Bordered</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="fa fa-angle-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Account ID</th>
                            <th>ID No</th>
                            <th>Customer Name</th>
                            <th>FEP</th>
                            <th>Entry</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($cc_reg as $cc_reg) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$cc_reg->AccountID}}</td>
                            <td>{{$cc_reg->IDNo}}</td>
                            <td>{{$cc_reg->CustName}}</td>
                            <td>{{$cc_reg->UserReg}}</td>
                            <td>{{$cc_reg->UserEnt}}</td>
                            <td><a href='#' class='btn btn-danger' data-toggle='modal' data-target='#myModal{{$i}}' style="font-size: 14px">Issue to entry </a>
                                 <div class="modal fade" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myModal{{$i}}" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                              </button>
                            </div>
                            <div class="modal-body"> 
                                <form method="POST"  id="smart-form-register3" class="form-horizontal" action="{{ url('admin/addncd')}}" >
                                {{ csrf_field() }}
                                    <fieldset>
                                            <label for="text1" class="control-label col-lg-4">Account ID</label>
                                            <div class="col-lg-8">
                                                <input type="text" id="text1" value="{{$cc_reg->AccountID}}" class="form-control">
                                            </div>
                                            <label for="text1" class="control-label col-lg-4">ID No</label>
                                            <div class="col-lg-8">
                                                <input type="text" id="text1" value="{{$cc_reg->IDNo}}" class="form-control">
                                            </div>
                                            <label for="text1" class="control-label col-lg-4">Customer Name</label>
                                            <div class="col-lg-8">
                                                <input type="text" id="text1" value="{{$cc_reg->CustName}}" class="form-control">
                                            </div>
                                            <label for="text1" class="control-label col-lg-4">User Id (Entry)</label>
                                            <div class="col-lg-8">
                                                <input type="text" id="text1" placeholder="Email" class="form-control">
                                            </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              </fieldset>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cancel
                              </button>
                              <button type="submit" name="submit" class="btn btn-primary">

                                Submit NCD to Customer
                              </button>
                          </form> 
                            </div>
                          </div>
                        </div>
                      </div>

                            </td>
                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>               
                </table>
            </div>
        </div>
    </div>

    
</div>
<!-- /.row -->
<!--End Datatables-->
<script type="text/javascript">
// Popup window code
function newPopup(url) {
    var win = window.open(
        url,'popUpWindow','height=1000px,width=1350px%,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');

    var win_timer = setInterval(function() {   
      if(win.closed) {
          window.location.reload();
          clearInterval(win_timer);
      } 
      }, 100); 

}
</script>
@endsection