@extends('public.master3')

@section('content')
<div class="row">
     <div class="col-lg-5">
        <div class="box">
            <header>
                <h5>Table Bordereds</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
                <table id="dataTable1" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Account ID & ID No</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($files as $file) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ substr($file,10,16) }} - {{ substr($file,27,12) }}</td>
                            <td> <a href="JavaScript:newPopup('{{url('/')}}/register/image/{{ substr($file,10,29) }}');" class='btn btn-danger'>Register</a></td>
                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>               
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
          <!--<iframe onload="document.getElementById('loadImg').style.display='none';" width="100%" height="400" src="{{url('/')}}/documents/CCA-2018-0062396_850418016087.pdf"></iframe>-->
           <div class="box">
            <header>
                <h5>Table Bordered</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Account ID</th>
                            <th>ID No</th>
                            <th>Customer Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($cc_reg as $data) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$data->AccountID}}</td>
                            <td>{{$data->IDNo}}</td>
                            <td>{{$data->CustName}}</td>
                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>               
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->
<script type="text/javascript">
// Popup window code
function newPopup(url) {
    var win = window.open(
        url,'popUpWindow','height=1000px,width=1350px%,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');

    var win_timer = setInterval(function() {   
      if(win.closed) {
          window.location.reload();
          clearInterval(win_timer);
      } 
      }, 100); 

}
</script>
@endsection