@extends('public.master3')

@section('content')
<div class="row">
     <div class="col-lg-5">
        <div class="box">
            <header>
                <h5>Table Bordered</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                            <i class="fa fa-angle-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </header>
            <div id="borderedTable" class="body collapse in">
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Account ID & ID No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($files as $file) 
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ substr($file,10,16) }} - {{ substr($file,27,12) }}</td>
                        </tr>
                        <?php  $i++; ?>
                        @endforeach
                    </tbody>               
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
          <iframe onload="document.getElementById('loadImg').style.display='none';" width="100%" height="400" src="{{url('/')}}/image"></iframe>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->
@endsection