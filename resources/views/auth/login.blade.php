<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login Page</title>
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
    
    <!-- Bootstrap -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/bootstrap/css/bootstrap.css') }}">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href=" {{ asset('assets/public/assets/lib/font-awesome/css/font-awesome.css') }}">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/css/main.css') }}">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/metismenu/metisMenu.css') }}">
    
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/onoffcanvas/onoffcanvas.css') }}">
    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/public/assets/lib/animate.css/animate.css') }}">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="login">

      <div class="form-signin">
    <div class="text-center">
        <img src="assets/img/logo.png" alt="Metis Logo">
    </div>
    <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
             <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                <p class="text-muted text-center">
                    Enter your username and password
                </p>
                 <input id="user_id" type="text" class="form-control top{{ $errors->has('email') ? ' is-invalid' : '' }}" name="user_id" placeholder="ID" value="{{ old('email') }}" required autofocus>

                  <input id="password" type="password" class="form-control bottom{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="PASSWORD" name="password" required>
                <div class="checkbox">
		  <label>
		  </label>
		</div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

                 <input type="button" onclick="BrowserDetection();" value="GetBrowswerName"/>
            </form>
        </div>
        <div id="forgot" class="tab-pane">
            <form action="index.html">
                <p class="text-muted text-center">Enter your valid e-mail</p>
                <input type="email" placeholder="mail@domain.com" class="form-control">
                <br>
                <button class="btn btn-lg btn-danger btn-block" type="submit">Recover Password</button>
            </form>
        </div>
        <div id="signup" class="tab-pane">
            <form action="index.html">
                <input type="text" placeholder="username" class="form-control top">
                <input type="email" placeholder="mail@domain.com" class="form-control middle">
                <input type="password" placeholder="password" class="form-control middle">
                <input type="password" placeholder="re-password" class="form-control bottom">
                <button class="btn btn-lg btn-success btn-block" type="submit">Register</button>
            </form>
        </div>
    </div>
    <hr>
    <div class="text-center">
        <ul class="list-inline">
            <li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>\
            <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li>
        </ul>
    </div>
  </div>


    <!--jQuery -->
    <script src="assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="assets/lib/bootstrap/js/bootstrap.js"></script>


    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('.list-inline li > a').click(function() {
                    var activeForm = $(this).attr('href') + ' > form';
                    //console.log(activeForm);
                    $(activeForm).addClass('animated fadeIn');
                    //set timer to 1 seconds, after that, unload the animate animation
                    setTimeout(function() {
                        $(activeForm).removeClass('animated fadeIn');
                    }, 1000);
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript">
        function BrowserDetection() {
                
                //Check if browser is IE or not
                if (navigator.userAgent.search("MSIE") >= 0) {
                    alert("Browser is InternetExplorer");
                }
                //Check if browser is Chrome or not
                else if (navigator.userAgent.search("Chrome") >= 0) {
                   window.location ="http://localhost:8080/dataentry/dataentry/public/assets/public/403.html";
                }
                //Check if browser is Firefox or not
                else if (navigator.userAgent.search("Firefox") >= 0) {
                    alert("Browser is FireFox");
                }
                //Check if browser is Safari or not
                else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
                    alert("Browser is Safari");
                }
                //Check if browser is Opera or not
                else if (navigator.userAgent.search("Opera") >= 0) {
                    alert("Browser is Opera");
                }
            }
    </script>
</body>

</html>
