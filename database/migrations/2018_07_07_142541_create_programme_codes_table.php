<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammeCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('app_programme_codes'))
        {
            Schema::create('app_programme_codes', function (Blueprint $table) {
                $table->increments('id');
                $table->String('programme_code_type',20)->nullable();
                $table->String('programme_code',20)->nullable();
                $table->String('programme_code_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_programme_codes');
    }
}
