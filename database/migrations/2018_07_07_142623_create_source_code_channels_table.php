<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceCodeChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('app_source_code_channels'))
        {
            Schema::create('app_source_code_channels', function (Blueprint $table) {
                $table->increments('id');
                $table->String('sc_code',20)->nullable();
                $table->String('app_sc',20)->nullable();
                $table->String('description',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_source_code_channels');
    }
}
