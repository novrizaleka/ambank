<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep5sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('temp_entry_steps5'))
        {
            Schema::create('temp_entry_steps5', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab5_mailing_address',20)->nullable();
                $table->String('temp_tab5_address_1',100)->nullable();
                $table->String('temp_tab5_address_2',100)->nullable();
                $table->String('temp_tab5_address_3',100)->nullable();
                $table->String('temp_tab5_postcode',100)->nullable();
                $table->String('temp_tab5_city',100)->nullable();
                $table->String('temp_tab5_state',100)->nullable();
                $table->String('temp_tab5_country',100)->nullable();
                $table->String('temp_tab5_home_phone',100)->nullable();
                $table->String('temp_tab5_mobile_phone',100)->nullable();
                $table->String('temp_tab5_email',100)->nullable();
                $table->String('temp_tab5_residence_type',100)->nullable();
                $table->integer('temp_tab5_year_stay')->nullable();
                $table->integer('temp_tab5_month_stay')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps5');
    }
}
