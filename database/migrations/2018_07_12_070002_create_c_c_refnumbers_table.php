<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCCRefnumbersTable extends Migration
{

     public function up()
    {
         if(!Schema::hasTable('register_cc_ref_numbers'))
        {
           Schema::create('register_cc_ref_numbers', function (Blueprint $table) {
                $table->increments('id');
                $table->String('refno',12)->nullable();
                $table->date('date_registration',20)->nullable();
                $table->String('user_id',10)->nullable();
                $table->boolean('sqlsvr')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_cc_ref_numbers');
    }
}
