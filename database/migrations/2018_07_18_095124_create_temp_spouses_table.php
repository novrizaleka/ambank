<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempSpousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          if(!Schema::hasTable('temp_spouses'))
        {
           Schema::create('temp_spouses', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id_no',100)->nullable();
                $table->Integer('id_main_temp')->nullable();
                $table->String('temp_spouse_id_type',20)->nullable();
                $table->String('temp_spouse_id_no',100)->nullable();
                $table->String('temp_spouse_alternate_id_type',100)->nullable();
                $table->String('temp_spouse_alternate_id_no',100)->nullable();
                $table->String('temp_spouse_name',100)->nullable();
                $table->String('temp_spouse_relationship',100)->nullable();
                $table->String('temp_spouse_name_company',100)->nullable();
                $table->String('temp_spouse_occupation',100)->nullable();
                $table->String('temp_spouse_other_occupation',100)->nullable();
                $table->String('temp_spouse_office_phone',100)->nullable();
                $table->String('temp_spouse_home_phone',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_spouses');
    }
}
