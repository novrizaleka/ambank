<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighRiskPostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('app_highrisk_postcodes'))
        {
            Schema::create('app_highrisk_postcodes', function (Blueprint $table) {
                $table->increments('id');
                $table->String('postcode_code',20)->nullable();
                $table->String('highrisk_postcode_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_highrisk_postcodes');
    }
}
