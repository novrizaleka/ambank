<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('temp_entry_steps4'))
        {
            Schema::create('temp_entry_steps4', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab4_id_type',20)->nullable();
                $table->String('temp_tab4_id_no',100)->nullable();
                $table->String('temp_tab4_alternate_id_type',100)->nullable();
                $table->String('temp_tab4_alternate_id_no',100)->nullable();
                $table->String('temp_tab4_customer_name',100)->nullable();
                $table->String('temp_tab4_salutation',100)->nullable();
                $table->String('temp_tab4_name_on_card',100)->nullable();
                $table->String('temp_tab4_sex',100)->nullable();
                $table->String('temp_tab4_race',100)->nullable();
                $table->String('temp_tab4_religion',100)->nullable();
                $table->String('temp_tab4_ethnic',100)->nullable();
                $table->String('temp_tab4_nationality',100)->nullable();
                $table->String('temp_tab4_citizenship',100)->nullable();
                $table->integer('temp_tab4_num_dependants')->nullable();
                $table->String('temp_tab4_marital_status',100)->nullable();
                $table->String('temp_tab4_edu_level',100)->nullable();
                $table->String('temp_tab4_ambank_staff',100)->nullable();
                $table->String('temp_tab4_vip',100)->nullable();
                $table->String('temp_tab4_mother_name',100)->nullable();
                $table->String('temp_tab4_temp_tab4_title',100)->nullable();
                $table->String('temp_tab4_dob',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps4');
    }
}
