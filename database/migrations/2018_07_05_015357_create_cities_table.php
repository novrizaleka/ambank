<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('ind_cities'))
        {
            Schema::create('ind_cities', function (Blueprint $table) {
                $table->increments('id');
                $table->String('country_code',2)->nullable();
                 $table->String('state_code',2)->nullable();
                $table->String('city_code',4)->nullable();
                $table->String('city_desc',50)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_cities');
    }
}
