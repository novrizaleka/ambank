<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('user_logs'))
        {
            Schema::create('user_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',10)->nullable();
                $table->String('ip',100)->nullable();
                $table->String('remark',100)->nullable();
                $table->String('type',100)->nullable();
                $table->date('date')->nullable();
                $table->datetime('date_log')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logs');
    }
}
