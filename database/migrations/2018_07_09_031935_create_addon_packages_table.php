<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('app_addon_packages'))
        {
            Schema::create('app_addon_packages', function (Blueprint $table) {
                    $table->increments('id');
                    $table->String('addon_package_code',20)->nullable();
                    $table->String('addon_package_code_conv',20)->nullable();
                    $table->String('addon_package_code_islamic',20)->nullable();
                    $table->String('addon_package_desc',100)->nullable();
                    $table->boolean('is_active')->default(1)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_addon_packages');
    }
}
