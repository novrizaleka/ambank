<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temp_entry_steps1'))
        {
            Schema::create('temp_entry_steps1', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('tab1_sc_channel_code',20)->nullable();
                $table->String('tab1_sc_branch',20)->nullable();
                $table->String('tab1_agent_type',120)->nullable();
                $table->String('tab1_agent_num',120)->nullable();
                $table->String('tab1_introducer',100)->nullable();
                $table->String('tab1_ecosway',100)->nullable();
                $table->String('tab1_ambank_protector',100)->nullable();
                $table->String('tab1_card_collection',100)->nullable();
                $table->String('tab1_card_delivery',100)->nullable();
                $table->String('tab1_collection_branch_state',100)->nullable();
                $table->String('tab1_collection_branch',100)->nullable();
                $table->String('tab1_plastic_data',100)->nullable();
                $table->String('tab1_business_owner',100)->nullable();
                $table->String('tab1_date_applied',100)->nullable();
                $table->String('tab1_aip_ref_no',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps1');
    }
}
