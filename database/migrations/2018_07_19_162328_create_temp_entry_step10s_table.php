<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep10sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('temp_entry_steps10'))
        {
            Schema::create('temp_entry_steps10', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_ref_id_type',20)->nullable();
                $table->String('temp_ref_id_no',100)->nullable();
                $table->String('temp_ref_alternate_id_type',100)->nullable();
                $table->String('temp_ref_alternate_id_no',100)->nullable();
                $table->String('temp_ref_name',100)->nullable();
                $table->String('temp_ref_relationship',100)->nullable();
                $table->String('temp_ref_name_company',100)->nullable();
                $table->String('temp_ref_occupation',100)->nullable();
                $table->String('temp_ref_other_occupation',100)->nullable();
                $table->String('temp_ref_office_phone',100)->nullable();
                $table->String('temp_ref_home_phone',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps10');
    }
}
