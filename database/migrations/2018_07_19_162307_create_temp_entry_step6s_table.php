<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep6sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('temp_entry_steps6'))
        {
            Schema::create('temp_entry_steps6', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab6_address_1',100)->nullable();
                $table->String('temp_tab6_address_2',100)->nullable();
                $table->String('temp_tab6_address_3',100)->nullable();
                $table->String('temp_tab6_postcode',100)->nullable();
                $table->String('temp_tab6_city',100)->nullable();
                $table->String('temp_tab6_state',100)->nullable();
                $table->String('temp_tab6_country',100)->nullable();
                $table->String('temp_tab6_home_phone',100)->nullable();
                $table->String('temp_tab6_residence_type',100)->nullable();
                $table->integer('temp_tab6_year_stay')->nullable();
                $table->integer('temp_tab6_month_stay')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('temp_entry_steps6');
    }
}
