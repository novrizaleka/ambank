<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('main_temporary_entries'))
        {
            Schema::create('main_temporary_entries', function (Blueprint $table) {
                    $table->increments('id');
                    $table->String('account',120)->nullable();
                    $table->String('user_id',20)->nullable();
                    $table->String('tab1_sc_channel_code',20)->nullable();
                    $table->String('tab1_sc_branch',20)->nullable();
                    $table->String('tab1_agent_type',120)->nullable();
                    $table->String('tab1_agent_num',120)->nullable();
                    $table->String('tab1_introducer',100)->nullable();
                    $table->String('tab1_ecosway',100)->nullable();
                    $table->String('tab1_ambank_protector',100)->nullable();
                    $table->String('tab1_card_collection',100)->nullable();
                    $table->String('tab1_card_delivery',100)->nullable();
                    $table->String('tab1_collection_branch_state',100)->nullable();
                    $table->String('tab1_collection_branch',100)->nullable();
                    $table->String('tab1_plastic_data',100)->nullable();
                    $table->String('tab1_business_owner',100)->nullable();
                    $table->String('tab1_date_applied',100)->nullable();
                    $table->String('tab1_aip_ref_no',100)->nullable();
                    //CHOICE OF CARD
                    $table->String('temp_tab2_product_type',20)->nullable();
                    $table->String('temp_tab2_category',100)->nullable();
                    $table->String('temp_tab2_promotion_prg',100)->nullable();
                    //CIF INFORMATION
                    $table->String('temp_tab4_id_type',20)->nullable();
                    $table->String('temp_tab4_id_no',100)->nullable();
                    $table->String('temp_tab4_alternate_id_type',100)->nullable();
                    $table->String('temp_tab4_alternate_id_no',100)->nullable();
                    $table->String('temp_tab4_customer_name',100)->nullable();
                    $table->String('temp_tab4_salutation',100)->nullable();
                    $table->String('temp_tab4_name_on_card',100)->nullable();
                    $table->String('temp_tab4_sex',100)->nullable();
                    $table->String('temp_tab4_race',100)->nullable();
                    $table->String('temp_tab4_religion',100)->nullable();
                    $table->String('temp_tab4_ethnic',100)->nullable();
                    $table->String('temp_tab4_nationality',100)->nullable();
                    $table->String('temp_tab4_citizenship',100)->nullable();
                    $table->integer('temp_tab4_num_dependants')->nullable();
                    $table->String('temp_tab4_marital_status',100)->nullable();
                    $table->String('temp_tab4_edu_level',100)->nullable();
                    $table->String('temp_tab4_ambank_staff',100)->nullable();
                    $table->String('temp_tab4_vip',100)->nullable();
                    $table->String('temp_tab4_mother_name',100)->nullable();
                    $table->String('temp_tab4_temp_tab4_title',100)->nullable();
                    $table->String('temp_tab4_dob',100)->nullable();
                    //MAILING ADDRESS
                    $table->String('temp_tab5_mailing_address',20)->nullable();
                    $table->String('temp_tab5_address_1',100)->nullable();
                    $table->String('temp_tab5_address_2',100)->nullable();
                    $table->String('temp_tab5_address_3',100)->nullable();
                    $table->String('temp_tab5_postode',100)->nullable();
                    $table->String('temp_tab5_city',100)->nullable();
                    $table->String('temp_tab5_state',100)->nullable();
                    $table->String('temp_tab5_country',100)->nullable();
                    $table->String('temp_tab5_home_phone',100)->nullable();
                    $table->String('temp_tab5_mobile_phone',100)->nullable();
                    $table->String('temp_tab5_email',100)->nullable();
                    $table->String('temp_tab5_residence_type',100)->nullable();
                    $table->integer('temp_tab5_year_stay')->nullable();
                    $table->integer('temp_tab5_month_stay')->nullable();
                    //PERMANENT ADDRESS
                    $table->String('temp_tab6_address_1',100)->nullable();
                    $table->String('temp_tab6_address_2',100)->nullable();
                    $table->String('temp_tab6_address_3',100)->nullable();
                    $table->String('temp_tab6_postode',100)->nullable();
                    $table->String('temp_tab6_city',100)->nullable();
                    $table->String('temp_tab6_state',100)->nullable();
                    $table->String('temp_tab6_country',100)->nullable();
                    $table->String('temp_tab6_home_phone',100)->nullable();
                    $table->String('temp_tab6_residence_type',100)->nullable();
                    $table->integer('temp_tab6_year_stay')->nullable();
                    $table->integer('temp_tab6_month_stay')->nullable();
                    //CURRENT EMP
                    $table->String('temp_tab7_emp_type',20)->nullable();
                    $table->String('temp_tab7_empr_name',100)->nullable();
                    $table->String('temp_tab7_department',100)->nullable();
                    $table->String('temp_tab7_address_1',100)->nullable();
                    $table->String('temp_tab7_address_2',100)->nullable();
                    $table->String('temp_tab7_address_3',100)->nullable();
                    $table->String('temp_tab7_postode',100)->nullable();
                    $table->String('temp_tab7_city',100)->nullable();
                    $table->String('temp_tab7_state',100)->nullable();
                    $table->String('temp_tab7_country',100)->nullable();
                    $table->String('temp_tab7_office_phone',100)->nullable();
                    $table->String('temp_tab7_fax_num',100)->nullable();
                    $table->String('temp_tab7_email',100)->nullable();
                    //CURRENT EMP2
                    $table->String('temp_tab8_nature_business',20)->nullable();
                    $table->String('temp_tab8_occupation',100)->nullable();
                    $table->String('temp_tab8_occupation_desc',100)->nullable();
                    $table->String('temp_tab8_job_sector',100)->nullable();
                    $table->String('temp_tab8_designation',100)->nullable();
                    $table->String('temp_tab8_address_3',100)->nullable();
                    $table->date('temp_tab8_date_joined',100)->nullable();
                    $table->integer('temp_tab8_year_emp')->nullable();
                    $table->integer('temp_tab8_month_emp')->nullable();
                    $table->String('temp_tab8_prev_company',100)->nullable();
                    $table->String('temp_tab8_prev_job_sector',100)->nullable();
                    $table->String('temp_tab8_prev_designation',100)->nullable();
                    $table->date('temp_tab8_prev_date_joined',100)->nullable();
                    $table->integer('temp_tab8_prev_year_emp')->nullable();
                    $table->integer('temp_tab8_prev_month_emp')->nullable();
                     //CURRENT EMP2
                    $table->boolean('temp_tablast_pi_vip')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_staff')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_info')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_serap')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_se_staff')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_fr')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_fo_pledge')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_doc_ic')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_doc_ot')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_bjr')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_pin_gen')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_direct_mail')->default(0)->nullable();
                    $table->boolean('temp_tablast_pi_res_ind')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_golf')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_travel')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_arts')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_beauty')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_health')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_dining')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_tech')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_ent')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_sports')->default(0)->nullable();
                    $table->boolean('temp_tablast_si_others')->default(0)->nullable();
                    $table->boolean('temp_tablast_excess_limit')->default(0)->nullable();
                    $table->integer('status')->default(0)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_temporary_entries');
    }
}
