<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStepLastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temp_entry_last'))
        {
            Schema::create('temp_entry_last', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('pi_vip')->nullable();
                $table->boolean('pi_staff')->nullable();
                $table->boolean('pi_info')->nullable();
                $table->boolean('pi_serap')->nullable();
                $table->boolean('pi_se_staff')->nullable();
                $table->boolean('pi_fr')->nullable();
                $table->boolean('pi_fo_plledge')->nullable();
                $table->boolean('pi_doc_ic')->nullable();
                $table->boolean('pi_doc_ot')->nullable();
                $table->boolean('pi_bjr')->nullable();
                $table->boolean('pi_pin_gen')->default(1)->nullable();
                $table->boolean('pi_direct_mailing')->nullable();
                $table->boolean('pi_res_ind')->nullable();


                $table->boolean('si_golf')->nullable();
                $table->boolean('si_travel')->nullable();
                $table->boolean('si_art_and_culture')->nullable();
                $table->boolean('si_beuty_and_fashion')->nullable();
                $table->boolean('si_health')->nullable();
                $table->boolean('si_dining')->nullable();
                $table->boolean('si_teach_and_gadget')->nullable();
                $table->boolean('si_entertaiment')->nullable();
                $table->boolean('si_sports')->nullable();
                $table->boolean('si_other')->nullable();

                $table->boolean('si_excess_of_card')->nullable();

                $table->timestamps();
            });
        }
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_last');
    }
}
