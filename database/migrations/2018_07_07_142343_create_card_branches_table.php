<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('app_card_branches'))
        {
            Schema::create('app_card_branches', function (Blueprint $table) {
                $table->increments('id');
                $table->String('card_branch_state',2)->nullable();
                $table->String('card_branch_code',20)->nullable();
                $table->String('card_branch_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_card_branches');
    }
}
