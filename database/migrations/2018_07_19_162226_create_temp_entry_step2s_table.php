<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temp_entry_steps2'))
        {
            Schema::create('temp_entry_steps2', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab2_product_type',20)->nullable();
                $table->String('temp_tab2_type',20)->nullable();
                $table->String('temp_tab2_category',100)->nullable();
                $table->String('temp_tab2_program_code',100)->nullable();
                $table->String('temp_tab2_card_type',100)->nullable();
                $table->String('temp_tab2_gift_code',100)->nullable();
                $table->String('temp_tab2_promotion_program',100)->nullable();
                $table->boolean('temp_tab2_zing_ind')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps2');
    }
}
