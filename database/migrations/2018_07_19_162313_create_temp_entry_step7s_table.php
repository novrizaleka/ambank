<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep7sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temp_entry_steps7'))
        {
            Schema::create('temp_entry_steps7', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab7_emp_type',20)->nullable();
                $table->String('temp_tab7_empr_name',100)->nullable();
                $table->String('temp_tab7_department',100)->nullable();
                $table->String('temp_tab7_address_1',100)->nullable();
                $table->String('temp_tab7_address_2',100)->nullable();
                $table->String('temp_tab7_address_3',100)->nullable();
                $table->String('temp_tab7_postcode',100)->nullable();
                $table->String('temp_tab7_city',100)->nullable();
                $table->String('temp_tab7_state',100)->nullable();
                $table->String('temp_tab7_country',100)->nullable();
                $table->String('temp_tab7_office_phone',100)->nullable();
                $table->String('temp_tab7_fax_num',100)->nullable();
                $table->String('temp_tab7_email',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps7');
    }
}
