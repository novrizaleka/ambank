<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEntryStep8sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('temp_entry_steps8'))
        {
            Schema::create('temp_entry_steps8', function (Blueprint $table) {
                $table->increments('id');
                $table->String('account_id',120)->nullable();
                $table->String('account_no',120)->nullable();
                $table->String('id_no',20)->nullable();
                $table->String('user_entry',10)->nullable();
                $table->integer('id_register')->nullable();
                $table->String('temp_tab8_nature_business',20)->nullable();
                $table->String('temp_tab8_occupation',100)->nullable();
                $table->String('temp_tab8_occupation_desc',100)->nullable();
                $table->String('temp_tab8_job_sector',100)->nullable();
                $table->String('temp_tab8_designation',100)->nullable();
                $table->String('temp_tab8_address_3',100)->nullable();
                $table->date('temp_tab8_date_joined',100)->nullable();
                $table->integer('temp_tab8_year_emp')->nullable();
                $table->integer('temp_tab8_month_emp')->nullable();
                $table->String('temp_tab8_prev_company',100)->nullable();
                $table->String('temp_tab8_prev_job_sector',100)->nullable();
                $table->String('temp_tab8_prev_designation',100)->nullable();
                $table->date('temp_tab8_prev_date_joined',100)->nullable();
                $table->integer('temp_tab8_prev_year_emp')->nullable();
                $table->integer('temp_tab8_prev_month_emp')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_entry_steps8');
    }
}
