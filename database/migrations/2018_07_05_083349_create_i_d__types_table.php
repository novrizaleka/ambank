<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIDTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('ind_id_types'))
        {
            Schema::create('ind_id_types', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('id_type_code')->nullable();
                $table->String('id_type_desc',50)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_id_types');
    }
}
