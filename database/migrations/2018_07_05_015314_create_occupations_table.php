<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ind_occupations'))
        {
            Schema::create('ind_occupations', function (Blueprint $table) {
                $table->increments('id');
                $table->String('occupation_type',2)->nullable();
                $table->String('occupation_code',4)->nullable();
                $table->String('occupation_desc',150)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_occupations');
    }
}
