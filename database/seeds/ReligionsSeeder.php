<?php

use Illuminate\Database\Seeder;
use App\Model\Parameter\Ind\Religions;

class ReligionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Religions::create([
            'id' => '1',
            'religion_code' => '1',
            'religion_desc' => 'Islam',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '2',
            'religion_code' => '2',
            'religion_desc' => 'Buddhist',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '3',
            'religion_code' => '3',
            'religion_desc' => 'Hindu',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '4',
            'religion_code' => '4',
            'religion_desc' => 'Christian',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '5',
            'religion_code' => '5',
            'religion_desc' => 'Sikh',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '6',
            'religion_code' => '6',
            'religion_desc' => 'Not Available',
            'is_active' => '1'
        ]);

        Religions::create([
            'id' => '9',
            'religion_code' => '9',
            'religion_desc' => 'Others',
            'is_active' => '1'
        ]);
    }
}
