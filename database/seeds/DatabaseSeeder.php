<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
          	$this->call(ReligionsSeeder::class);
        	$this->call(RacesSeeder::class);
        	$this->call(RelationshipsSeeder::class);
        	$this->call(SexsSeeder::class);
    }
}
