<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Parameter\Ind\Salutations;
use App\Model\Parameter\Ind\Sexs;
use App\Model\Parameter\Ind\ID_Types;
use App\Model\Parameter\Ind\Titles;
use App\Model\Parameter\Ind\Religions;
use App\Model\Parameter\Ind\Ethnics;
use App\Model\Parameter\Ind\Races;
use App\Model\Parameter\Ind\AmbankStaffs;
use App\Model\Parameter\Ind\ApplicantTypes;
use App\Model\Parameter\Ind\Cities;
use App\Model\Parameter\Ind\Citizenships;
use App\Model\Parameter\Ind\Countries;
use App\Model\Parameter\Ind\Designations;
use App\Model\Parameter\Ind\Educations;
use App\Model\Parameter\Ind\Employement_Types;
use App\Model\Parameter\Ind\Job_sectors;
use App\Model\Parameter\Ind\Mailing_addresses;
use App\Model\Parameter\Ind\Marital_status;
use App\Model\Parameter\Ind\Nationalities;
use App\Model\Parameter\Ind\Occupations;
use App\Model\Parameter\Ind\NaturalBusinesses;
use App\Model\Parameter\Ind\Recidences;
use App\Model\Parameter\Ind\Relationships;
use App\Model\Parameter\Ind\States;
use App\Model\Parameter\Ind\Vip;

use DB;
use Input;
use Response;

class ApiCIFController extends Controller
{
    public function idtype(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_id_types')
                ->where('id_type_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->id, 'value' => $query->id_type_desc ];
            }
            return Response::json($results);
        }
    }

    public function idtypealt(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_id_types')
                ->where('id_type_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->id, 'value' => $query->id_type_desc ];
            }
            return Response::json($results);
        }
    }

    public function salutation(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_salutations')
                ->where('salutation_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->salutation_code, 'value' => $query->salutation_desc ];
            }
            return Response::json($results);
        }
    }

    public function sex(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_sexs')
                ->where('sexs_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->sexs_code, 'value' => $query->sexs_desc ];
            }
            return Response::json($results);
        }
    }

    public function race(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_races')
                ->where('race_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->race_code, 'value' => $query->race_desc ];
            }
            return Response::json($results);
        }
    }

    public function religion(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_religions')
                ->where('religion_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->religion_code, 'value' => $query->religion_desc ];
            }
            return Response::json($results);
        }
    }

    public function ethnic(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_ethnics')
                ->where('ethnic_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->ethnic_code, 'value' => $query->ethnic_desc ];
            }
            return Response::json($results);
        }
    }

    public function nationality(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_nationalities')
                ->where('nationality_code_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->nationality_code, 'value' => $query->nationality_code_desc ];
            }
            return Response::json($results);
        }
    }

    public function citizenship(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_citizenships')
                ->where('citizenship_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->citizenship_code, 'value' => $query->citizenship_desc ];
            }
            return Response::json($results);
        }
    }

    public function marital(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_marital_status')
                ->where('marital_status_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->marital_status_code, 'value' => $query->marital_status_desc ];
            }
            return Response::json($results);
        }
    }

    public function education(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_educations')
                ->where('education_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->education_code, 'value' => $query->education_desc ];
            }
            return Response::json($results);
        }
    }

    public function ambank_staff(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_ambank_staffs')
                ->where('ambank_staff_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->ambank_staff_code, 'value' => $query->ambank_staff_desc ];
            }
            return Response::json($results);
        }
    }

    public function vip(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_vip')
                ->where('vip_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->vip_code, 'value' => $query->vip_desc ];
            }
            return Response::json($results);
        }
    }

    public function title(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_titles')
                ->where('title_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->title_code, 'value' => $query->title_desc ];
            }
            return Response::json($results);
        }
    }

    public function mailing(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_mailing_addresses')
                ->where('mailing_address_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->mailing_address_code, 'value' => $query->mailing_address_desc ];
            }
            return Response::json($results);
        }
    }

    public function residence(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_residences')
                ->where('residence_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->residence_code, 'value' => $query->residence_desc ];
            }
            return Response::json($results);
        }
    }

    public function emp_type(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_employement_types')
                ->where('employement_type_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->employement_type_code, 'value' => $query->employement_type_desc ];
            }
            return Response::json($results);
        }
    }

    public function nature(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_nature_of_businesses')
                ->where('nature_of_business_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->nature_of_business_code, 'value' => $query->nature_of_business_desc ];
            }
            return Response::json($results);
        }
    }

    public function occupation(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_occupations')
                ->where('occupation_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->occupation_code, 'value' => $query->occupation_desc ];
            }
            return Response::json($results);
        }
    }

    public function job_sector(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_job_sectors')
                ->where('job_sector_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->job_sector_code, 'value' => $query->job_sector_desc ];
            }
            return Response::json($results);
        }
    }

    public function designation(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_designations')
                ->where('designation_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->designation_code, 'value' => $query->designation_desc ];
            }
            return Response::json($results);
        }
    }

    public function relationship(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('ind_relationships')
                ->where('relationship_desc', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = ['id' => $query->relationship_code, 'value' => $query->relationship_desc ];
            }
            return Response::json($results);
        }
    }

}
