<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRefnumbers;
use App\Model\Register\CCRegisters;
use Auth;
use App\User;
use File;
use Storage;
class IssueToDataEntryController extends Controller
{
     public function index()
    { 
    	$user   = User::orderBy('created_at','DESC')->get();
    	$cc_reg   = CCRegisters::orderBy('created_at','DESC')->get();
    	return view('public.register.issue_entry',compact('cc_reg'));
    }

    public function autocomplete(Request $request)
{

    $term = $request->input('user_id');
      $data=User::where('user_id','LIKE','%'.$term.'%')->take(10)->get();
      //var_dump($data);

      $results=array();


      foreach ($data as $key => $v) {

          $results[]=['value'=>$v->user_id];

      }

      return response()->json($results);

  }

  

  	public function save_issue_entry(Request $request)
  	{
  		$id 	        = $request->input('id');
  		$account_no 	= $request->input('account_no');
      $account_id_no   = $request->input('account_id_no');
        $cust_name      = $request->input('cust_name');
        $id_no          = $request->input('id_no');
  		$today          = date('Y-m-d H:i:s');
  	
	  	$CCRegisters = CCRegisters::Where('id',$id)->update([
	       "UserEnt"	   => $request->user_id,
	       "DateEntrySt" => $today
	    ]);
       $cc_reg   = CCRegisters::latest('created_at')->where('id', $id)->limit('1')->first();
       $refno = $cc_reg->REG_RefNo;
	  	$validate           = TemporaryEntries::latest('created_at')->where('account', $account_no)->count();
        
        if ($validate == 0) 
            {
                $temporary  = new TemporaryEntries;
		          $temporary->account                     = $account_id_no;
                $temporary->temp_tab4_customer_name     = $cust_name;
                $temporary->temp_tab4_id_no             = $id_no;
                $temporary->user_id                     = $request->user_id;
		        $temporary->save();
            }
        
             $new_path = public_path().'/documents/1.pdf';
        $old_path = public_path().'/process_folder/'. $refno.'/1.pdf';
        $move = File::move($new_path, $old_path);


/*Storage::move( public_path().'/documents/'.$account_id_no,'/process_folder/'. $refno.'/'.$account_id_no);*/
	    return redirect('/issue_to_entry')->with('message', 'success!!');
  	}
}
