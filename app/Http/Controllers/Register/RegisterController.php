<?php
namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use File;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRefnumbers;
use App\Model\Register\CCRegisters;
use Auth;
use App\User;
use App\Model\Log\UserLogs;
use DateTime;
use DateTimeZone;
use App\Model\Entry\TempEntryStep1;
use App\Model\Entry\TempEntryStep2;
use App\Model\Entry\TempEntryStep3;
use App\Model\Entry\TempEntryStep4;
use App\Model\Entry\TempEntryStep5;
use App\Model\Entry\TempEntryStep6;
use App\Model\Entry\TempEntryStep7;
use App\Model\Entry\TempEntryStep8;
use App\Model\Entry\TempEntryStep9;
use App\Model\Entry\TempEntryStep10;


class RegisterController extends Controller
{
     public function inde2x()
    {
        return view('public.entry.register_entry');
    }

    public function index()
    { 
        $files = File::allFiles('documents'); 
        $cc_reg   = CCRegisters::orderBy('created_at','DESC')->get();
        return view('public.register.register',compact('files','cc_reg'));
    }
    public function table()
    { 
        $today    = date('Y-m-d');
        $files = File::allFiles('documents'); 

        $lastmodified = File::lastModified('documents');
        $lastmodified = DateTime::createFromFormat("U", $lastmodified)->setTimezone(new DateTimeZone('America/New_York'));
        $lastmodified = $lastmodified->format('Y-m-d H:i:s');

        $cc_reg   = CCRegisters::orderBy('created_at','DESC')->get();
        $user = User::orderBy('created_at','DESC')->where('role','2')->get();
        $user_log = UserLogs::orderBy('created_at','DESC')->groupby('user_id')->where('type','1')->where('date',$today)->get();
        return view('public.register.register_table',compact('files','cc_reg','user','user_log','lastmodified'));
    }

    public function image(Request $request) 
    {
        $account = $request->input('account');
        $file = $request->input('file');
        $link = $request->input('link');
        $temporary  = new TemporaryEntries;
        $temporary->account     = $link;
        $temporary->file     = $file;
        $temporary->save();
        
        return redirect('/register')->with('message', 'success!!');
    }

    public function image_view($id) 
    {
        $link      = $id;
        $cc_reg   = CCRegisters::latest('created_at')->where('account_id_no',$id)->limit('1')->first();
        return view('public.register.register_image',compact('link','cc_reg'));
    }

     public function done_register_image(Request $request) 
    {
        $user = Auth::user();

        $refno      = $request->input('reference_number');
        $account    = $request->input('account_no');
        $id_no      = $request->input('id_no');
        $link       = $request->input('link');
         $file         = $request->input('file');
        $cust_name  = $request->input('customer_name');
        $today      = date('Y-m-d H:i:s');
        $user_id    = $user->user_id;

        $validate   = CCRefnumbers::latest('created_at')->where('refno', $refno)->count();
        
        if ($validate == 0) 
            {
                $cc_ref                     = new CCRefnumbers;
                $cc_ref->refno              = $refno;
                $cc_ref->date_registration  = $today;
                $cc_ref->user_id            = $user_id;
                $cc_ref->save();

                $path = public_path().'/process_folder/'.$refno;
                File::makeDirectory($path, $mode = 0777, true, true);
            }
        
        $cc_reg                     = new CCRegisters;
        $cc_reg->REG_RefNo          = $refno;
        $cc_reg->account_id_no      = $link;
        $cc_reg->AccountID          = $account;
        $cc_reg->IDNo               = $id_no;
        $cc_reg->CustName           = $cust_name;
        $cc_reg->DateReceived       = $today;
        $cc_reg->DateRegister       = $today;
        $cc_reg->UserReg            = $user_id;
        $cc_reg->save();

        $new_path = public_path().'/documents/'.$file;
        $old_path = public_path().'/process_folder/'. $refno.'/'.$file;
        $move = File::move($new_path, $old_path);

        return redirect('/register')->with('message', 'success!!');
    }


    public function done_register_table(Request $request) 
    {
        $user = Auth::user();
        $user     = $user->user_id;
        $refno      = $request->input('reference_number');
        $account    = $request->input('account_no');
        $account_id_no   = $request->input('account_id_no');
        $id_no      = $request->input('id_no');
        $link       = $request->input('link');
        $file      = $request->input('file');
        $cust_name  = $request->input('customer_name');
        $today      = date('Y-m-d H:i:s');
        $user_id    = $request->input('user');

        $validate   = CCRefnumbers::latest('created_at')->where('refno', $refno)->count();
        
        if ($validate == 0) 
            {
                $cc_ref                     = new CCRefnumbers;
                $cc_ref->refno              = $refno;
                $cc_ref->date_registration  = $today;
                $cc_ref->save();

                $path = public_path().'/process_folder/'.$refno;
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            
            $item = array_map(null, $request->account_id,$request->id,$request->file,$request->date_rcv,$request->refnum);

            foreach($item as $val) {
                if($val[0] != ''){


                $cc_reg                     = new CCRegisters;
                $cc_reg->REG_RefNo          = $val[4];
                $cc_reg->account_id_no      = $val[2];
                $cc_reg->AccountID          = $val[0];
                $cc_reg->IDNo               = $val[1];
                $cc_reg->DateReceived       = $val[3];
                $cc_reg->DateRegister       = $today;
                $cc_reg->UserReg            = $user;
                $cc_reg->UserEnt           = $user_id;
                $cc_reg->save();

                $tempstep1              = new TempEntryStep1;
                $tempstep2              = new TempEntryStep2;
                $tempstep3              = new TempEntryStep3;
                $tempstep4              = new TempEntryStep4;
                $tempstep5              = new TempEntryStep5;
                $tempstep6              = new TempEntryStep6;
                $tempstep7              = new TempEntryStep7;
                $tempstep8              = new TempEntryStep8;
                $tempstep9              = new TempEntryStep9;
                $tempstep10             = new TempEntryStep10;

                
                $tempstep1->ccreg()->associate($cc_reg);
                $tempstep1->account_id   =  $val[2];
                $tempstep1->account_no   =  $val[0];
                $tempstep1->id_no        =  $val[1];
                $tempstep1->user_entry   =  $user_id;
                $tempstep1->save();

                $tempstep2->ccreg()->associate($cc_reg);
                $tempstep2->account_id   =  $val[2];
                $tempstep2->account_no   =  $val[0];
                $tempstep2->id_no        =  $val[1];
                $tempstep2->user_entry   =  $user_id;
                $tempstep2->save();

                $tempstep3->ccreg()->associate($cc_reg);
                $tempstep3->account_id   =  $val[2];
                $tempstep3->account_no   =  $val[0];
                $tempstep3->id_no        =  $val[1];
                $tempstep3->user_entry   =  $user_id;
                $tempstep3->save();

                $tempstep4->ccreg()->associate($cc_reg);
                $tempstep4->account_id   =  $val[2];
                $tempstep4->account_no   =  $val[0];
                $tempstep4->id_no        =  $val[1];
                $tempstep4->user_entry   =  $user_id;
                $tempstep4->save();

                $tempstep5->ccreg()->associate($cc_reg);
                $tempstep5->account_id   =  $val[2];
                $tempstep5->account_no   =  $val[0];
                $tempstep5->id_no        =  $val[1];
                $tempstep5->user_entry   =  $user_id;
                $tempstep5->save();

                $tempstep6->ccreg()->associate($cc_reg);
                $tempstep6->account_id   =  $val[2];
                $tempstep6->account_no   =  $val[0];
                $tempstep6->id_no        =  $val[1];
                $tempstep6->user_entry   =  $user_id;
                $tempstep6->save();

                $tempstep7->ccreg()->associate($cc_reg);
                $tempstep7->account_id   =  $val[2];
                $tempstep7->account_no   =  $val[0];
                $tempstep7->id_no        =  $val[1];
                $tempstep7->user_entry   =  $user_id;
                $tempstep7->save();

                $tempstep8->ccreg()->associate($cc_reg);
                $tempstep8->account_id   =  $val[2];
                $tempstep8->account_no   =  $val[0];
                $tempstep8->id_no        =  $val[1];
                $tempstep8->user_entry   =  $user_id;
                $tempstep8->save();

                $tempstep9->ccreg()->associate($cc_reg);
                $tempstep9->account_id   =  $val[2];
                $tempstep9->account_no   =  $val[0];
                $tempstep9->id_no        =  $val[1];
                $tempstep9->user_entry   =  $user_id;
                $tempstep9->save();

                $tempstep10->ccreg()->associate($cc_reg);
                $tempstep10->account_id   =  $val[2];
                $tempstep10->account_no   =  $val[0];
                $tempstep10->id_no        =  $val[1];
                $tempstep10->user_entry   =  $user_id;
                $tempstep10->save();
                $new_path = public_path().'/documents/'.$val[2];
                $old_path = public_path().'/process_folder/'. $refno.'/'.$val[2];
                $move = File::move($new_path, $old_path);
            }
              }
            
           
          /*$new_path = public_path().'/documents/'.$file;
        $old_path = public_path().'/process_folder/'. $refno.'/'.$file;
        $move = File::move($new_path, $old_path);*/

        return redirect('/register/table')->with('message', 'success!!');
    }

}
