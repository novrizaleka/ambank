<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Parameter\App\AgentNo;
use App\Model\Parameter\App\AgentTypes;
use App\Model\Parameter\App\CardBranches;
use App\Model\Parameter\App\CardBranchStates;
use App\Model\Parameter\App\ProgrammeCodes;
use DB;
use Input;
use App\Model\Parameter\App\SourceCodeChannels;
use App\Model\Parameter\App\SourceCodeBranches;
use App\Model\Parameter\Postcode;
use Response;

class ApiController extends Controller
{
     public function agent_type($id)
    {
        // $data = CarModel::where('id_brand',$id)->get();
        $data = AgentTypes::join('app_agent_numbers', 'app_agent_numbers.agent_number_type', '=', 'app_agent_types.agent_type_code')
        ->where('app_agent_numbers.agent_number_type',$id)->get(); 
        return $data;
    }

    public function card_branch_state($id)
    {
        // $data = CarModel::where('id_brand',$id)->get();
        $data = CardBranchStates::join('app_card_branches', 'app_card_branches.card_branch_state', '=', 'app_card_branch_states.card_branch_state_code')
        ->where('app_card_branches.card_branch_state',$id)->get(); 
        return $data;
    }

     public function product_type($id)
    {
        // $data = CarModel::where('id_brand',$id)->get();
        $data = ProgrammeCodes::where('app_programme_codes.programme_code',$id)->get(); 
        return $data;
    }

    public function sc_channel(Request $request)
    {
      if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_source_code_channels')
                ->where('sc_code', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->sc_code ];
            }
            return Response::json($results);
        }
    }

    public function sc_branch(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_source_code_branches')
                ->where('source_code_branche_code', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->source_code_branche_code ];
            }
            return Response::json($results);
        }
    }

    public function agenttype(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_agent_types')
                ->where('agent_type_code', 'LIKE', '%'.$term.'%')
                ->take(15)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->agent_type_code ];
            }
            return Response::json($results);
        }
    }

    public function agentnumber(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_agent_numbers')
                ->where('agent_number_code', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->agent_number_code ];
            }
            return Response::json($results);
        }
    }

    public function carddelivery(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_card_deliveries')
                ->where('card_delivery_desc', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->card_delivery_desc ];
            }
            return Response::json($results);
        }
    }

    public function cardcollection(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_card_collections')
                ->where('card_collection_desc', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->card_collection_desc ];
            }
            return Response::json($results);
        }
    }

    public function cardbranchstate(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_card_branch_states')
                ->where('card_branch_state_desc', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->card_branch_state_code, 'label' => $query->card_branch_state_desc ];
            }
            return Response::json($results);
        }
    }

    public function plasticdata(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_plastic_datas')
                ->where('plastic_data_desc', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->plastic_data_desc ];
            }
            return Response::json($results);
        }
    }

    public function businessowner(Request $request)
    {
        if ($request->ajax()) 
        {
            $term = Input::get('term');

            $results = array();  

            $queries = DB::table('app_business_owners')
                ->where('business_owner_desc', 'LIKE', '%'.$term.'%')
                ->take(5)->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->business_owner_desc ];
            }
            return Response::json($results);
        }
    }

    public function postcode($id)
    {
        $data = Postcode::where('postcode',$id)->with('state')->limit(1)->get();
        return $data;
    }
}
