<?php

namespace App\Http\Controllers\DataEntry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRefnumbers;
use App\Model\Register\CCRegisters;
use View;
use File;
use Auth;
class DataEntryController extends Controller
{
     public function inde2x()
    {
        return view('public.entry.register_entry');
    }

    public function indexs()
    { 
    	$files = File::allFiles('documents'); 
    	return view('public.entry.register_entry',compact('files'));
    }

     public function index()
    { 
    	$user = Auth::user();
    	$user_id = $user->user_id;
    	 $cc_reg   = CCRegisters::latest('created_at')->where('UserEnt', $user_id)->get();
    	$files = File::allFiles('documents'); 
    	return view('public.entry.data_entry',compact('files','cc_reg','user_id'));
    }

}
