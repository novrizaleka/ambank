<?php

namespace App\Http\Controllers\DataEntry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Parameter\Ind\Salutations;
use App\Model\Parameter\Ind\Sexs;
use App\Model\Parameter\Ind\ID_Types;
use App\Model\Parameter\Ind\Titles;
use App\Model\Parameter\Ind\Religions;
use App\Model\Parameter\Ind\Ethnics;
use App\Model\Parameter\Ind\Races;
use App\Model\Parameter\Ind\AmbankStaffs;
use App\Model\Parameter\Ind\ApplicantTypes;
use App\Model\Parameter\Ind\Cities;
use App\Model\Parameter\Ind\Citizenships;
use App\Model\Parameter\Ind\Countries;
use App\Model\Parameter\Ind\Designations;
use App\Model\Parameter\Ind\Educations;
use App\Model\Parameter\Ind\Employement_Types;
use App\Model\Parameter\Ind\Job_sectors;
use App\Model\Parameter\Ind\Mailing_addresses;
use App\Model\Parameter\Ind\Marital_status;
use App\Model\Parameter\Ind\Nationalities;
use App\Model\Parameter\Ind\Occupations;
use App\Model\Parameter\Ind\NaturalBusinesses;
use App\Model\Parameter\Ind\Recidences;
use App\Model\Parameter\Ind\Relationships;
use App\Model\Parameter\Ind\States;
use App\Model\Parameter\Ind\Vip;
use App\Model\Parameter\App\AddonPackages;
use App\Model\Parameter\App\AddonPlans;
use App\Model\Parameter\App\AgentNo;
use App\Model\Parameter\App\AgentTypes;
use App\Model\Parameter\App\ApplicationCategories;
use App\Model\Parameter\App\BusinessOwners;
use App\Model\Parameter\App\CardBranches;
use App\Model\Parameter\App\CardBranchStates;
use App\Model\Parameter\App\CardCollections;
use App\Model\Parameter\App\CardDeliveries;
use App\Model\Parameter\App\CardTypes;
use App\Model\Parameter\App\FeeCodes;
use App\Model\Parameter\App\GiftCodes;
use App\Model\Parameter\App\HighRiskPostcodes;
use App\Model\Parameter\App\Introducers;
use App\Model\Parameter\App\PlasticDatas;
use App\Model\Parameter\App\ProgrammeCodes;
use App\Model\Parameter\App\PromotionPrograms;
use App\Model\Parameter\App\SourceCodeBranches;
use App\Model\Parameter\App\SourceCodeChannels;
use App\Model\Parameter\App\TypeProducts;
use Auth;
use App\User;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRegisters;
use App\Model\Entry\TempEntryStep2;
class Step2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function step2($account, $view=FALSE)
    {
        $user = Auth::User();
       
        //app
        $addon_package      = AddonPackages::orderby('id','ASC')->get();
        $addon_plan         = AddonPlans::orderby('id','ASC')->get();
        $agent_no           = AgentNo::orderby('id','ASC')->get();
        $agent_type         = AgentTypes::orderby('id','ASC')->get();
        $applicant_category = ApplicationCategories::orderby('id','ASC')->get();
        $business_owner     = BusinessOwners::orderby('id','ASC')->get();
        $card_branch        = CardBranches::orderby('id','ASC')->get();
        $card_branch_state  = CardBranchStates::orderby('id','ASC')->get();
        $card_collection    = CardCollections::orderby('id','ASC')->get();
        $card_delivery      = CardDeliveries::orderby('id','ASC')->get();
        $card_type          = CardTypes::orderby('id','ASC')->get();
        $fee_code           = FeeCodes::orderby('id','ASC')->get();
        $gift_code          = GiftCodes::orderby('id','ASC')->where('id','27')->get();
         $card_type2          = CardTypes::orderby('id','ASC')->get();
        $fee_code2           = FeeCodes::orderby('id','ASC')->get();
        $gift_code2          = GiftCodes::orderby('id','ASC')->where('id','27')->get();
        $highrisk_postcode  = HighRiskPostcodes::orderby('id','ASC')->get();
        $introducer         = Introducers::orderby('id','ASC')->get();
        $plastic_data       = PlasticDatas::orderby('id','ASC')->get();
        $programme_code     = ProgrammeCodes::orderby('id','ASC')->get();
        $product_type       = ProgrammeCodes::select('programme_code')->groupby('programme_code')->get();
        $promotion_program  = PromotionPrograms::orderby('id','ASC')->get();
        $sc_branch          = SourceCodeBranches::orderby('id','ASC')->get();
        $sc_channel         = SourceCodeChannels::orderby('id','ASC')->get();
        $type_product         = TypeProducts::orderby('id','ASC')->get();

        $today              = date('dmy');
         $temp_entry         = TemporaryEntries::where('account',$account)->limit('1')->first();
         $cc_reg             = CCRegisters::latest('created_at')->where('account_id_no',$account)->limit('1')->first();

        return view('public.entry.entry_step2', compact('salutation','sex','id_type','race','religion','ambank_staff','applicant_type','city','citizenship','designation','country','education','employement_type','ethnic','job_sector','mailing_address','marital','nationality','naturebusiness','occupation','residence','relationship','state','title','vip','addon_package','addon_plan','agent_no','agent_type','applicant_category','business_owner','card_branch','card_branch_state','card_collection','card_delivery','card_type','fee_code','gift_code','highrisk_postcode','introducer','plastic_data','promotion_program','sc_branch','sc_channel','today','programme_code','salutation_spo','sex_spo','id_type_spo','race_spo','religion_spo','ambank_staff_spo','applicant_type_spo','city_spo','citizenship_spo','designation_spo','country_spo','education_spo','employement_type','ethnic','job_sector_spo','mailing_address_spo','marital','nationality_spo','naturebusiness_spo','occupation_spo','residence_spo','relationship_spo','state_spo','title_spo','vip_spo','salutation_sup','sex_sup','id_type_sup','race_sup','religion_sup','ambank_staff_sup','applicant_type_sup','city_sup','citizenship_sup','designation_sup_sup','country_sup_sup','education_sup','employement_type_sup','ethnic_sup','job_sector_sup','mailing_address_sup','marital_sup','nationality_sup','naturebusiness_sup','occupation_sup','residence_sup','relationship_sup','state_sup','title_sup','vip_sup','product_type','card_type2','fee_code2','gift_code2','temp_entry','type_product','cc_reg'
        )); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_step2(Request $request)
    {

        $user = Auth::User();
        $account_id = $request->input('account');
        $account_no = $request->input('account_no');
        $id_no = $request->input('id_no');
        $item = array_map(null, $request->product_type,$request->type,$request->card_type,$request->gift,$request->zing);

            foreach($item as $val) {
                

                $step2                          = new TempEntryStep2;
                $step2->account_id              = $account_id;
                $step2->account_no              = $account_no;
                $step2->id_no                   = $id_no;
                $step2->user_entry              = $user->user_id;
                $step2->temp_tab2_product_type  = $val[0];
                $step2->temp_tab2_type          = $val[1];
                //$step2->temp_tab2_program_code  = $val[2];
                $step2->temp_tab2_card_type     = $val[2];
                $step2->temp_tab2_gift_code     = $val[3];
                $step2->temp_tab2_promotion_program = $promotion_program;
                $step2->temp_tab2_zing_ind      =  $val[4];;
                $cc_reg->save();
       
    } 
      

        return redirect('/data_entry/step_1');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {      
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account)
    {
        $user = Auth::User();
        $account = $request->input('account');

        $temp   = TemporaryEntries::where('account',$account)->update([
            "tab1_sc_channel_code"      => $request->sc_channel,
            "tab1_sc_branch"          => $request->sc_branch,
            "tab1_agent_type"         => $request->agent_type,
            "tab1_agent_num"          => $request->agent_no,
            "tab1_introducer"         => $request->agent_name,
            "tab1_ecosway"          => $request->ecosway,
            "tab1_ambank_protector"     => $request->introducer,
            "tab1_card_collection"      => $request->card_collection,
            "tab1_card_delivery"        => $request->card_delivery,
            "tab1_collection_branch_state"  => $request->card_branch_state,
            "tab1_collection_branch"      => $request->card_branch,
            "tab1_plastic_data"         => $request->plastic_data,
            "tab1_business_owner"       => $request->business_owner,
            "tab1_date_applied"         => $request->date_applied,
            "tab1_ambank_protector"     => $request->ambank_protector,
            "tab1_aip_ref_no"         => $request->aip_refno
    ]);

        return redirect('/data_entry/step_2');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
