<?php

namespace App\Http\Controllers\DataEntry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Parameter\Ind\Salutations;
use App\Model\Parameter\Ind\Sexs;
use App\Model\Parameter\Ind\ID_Types;
use App\Model\Parameter\Ind\Titles;
use App\Model\Parameter\Ind\Religions;
use App\Model\Parameter\Ind\Ethnics;
use App\Model\Parameter\Ind\Races;
use App\Model\Parameter\Ind\AmbankStaffs;
use App\Model\Parameter\Ind\ApplicantTypes;
use App\Model\Parameter\Ind\Cities;
use App\Model\Parameter\Ind\Citizenships;
use App\Model\Parameter\Ind\Countries;
use App\Model\Parameter\Ind\Designations;
use App\Model\Parameter\Ind\Educations;
use App\Model\Parameter\Ind\Employement_Types;
use App\Model\Parameter\Ind\Job_sectors;
use App\Model\Parameter\Ind\Mailing_addresses;
use App\Model\Parameter\Ind\Marital_status;
use App\Model\Parameter\Ind\Nationalities;
use App\Model\Parameter\Ind\Occupations;
use App\Model\Parameter\Ind\NaturalBusinesses;
use App\Model\Parameter\Ind\Recidences;
use App\Model\Parameter\Ind\Relationships;
use App\Model\Parameter\Ind\States;
use App\Model\Parameter\Ind\Vip;
use App\Model\Parameter\App\AddonPackages;
use App\Model\Parameter\App\AddonPlans;
use App\Model\Parameter\App\AgentNo;
use App\Model\Parameter\App\AgentTypes;
use App\Model\Parameter\App\ApplicationCategories;
use App\Model\Parameter\App\BusinessOwners;
use App\Model\Parameter\App\CardBranches;
use App\Model\Parameter\App\CardBranchStates;
use App\Model\Parameter\App\CardCollections;
use App\Model\Parameter\App\CardDeliveries;
use App\Model\Parameter\App\CardTypes;
use App\Model\Parameter\App\FeeCodes;
use App\Model\Parameter\App\GiftCodes;
use App\Model\Parameter\App\HighRiskPostcodes;
use App\Model\Parameter\App\Introducers;
use App\Model\Parameter\App\PlasticDatas;
use App\Model\Parameter\App\ProgrammeCodes;
use App\Model\Parameter\App\PromotionPrograms;
use App\Model\Parameter\App\SourceCodeBranches;
use App\Model\Parameter\App\SourceCodeChannels;
use Auth;
use App\User;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRegisters;//copy
use App\Model\Entry\TempEntryStep6;//copy

class Step6Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function step6($account, $view=FALSE)
    {
        $user = Auth::User();
        $salutation 		= Salutations::orderby('id','ASC')->get();
    	$sex 				= Sexs::orderby('id','ASC')->get();
    	$id_type 			= ID_Types::orderby('id','ASC')->get();
    	$race 		 		= Races::orderby('id','ASC')->get();
    	$religion 		 	= Religions::orderby('id','ASC')->get();
    	$ambank_staff 		= AmbankStaffs::orderby('id','ASC')->get();
    	$applicant_type 	= ApplicantTypes::orderby('id','ASC')->get();
    	$city 		 		= Cities::orderby('id','ASC')->get();
    	$citizenship 		= Citizenships::orderby('id','ASC')->get();
    	$designation 		= Designations::orderby('id','ASC')->get();
    	$country 		 	= Countries::orderby('id','ASC')->get();
    	$education 		 	= Educations::orderby('id','ASC')->get();
    	$employement_type 	= Employement_Types::orderby('id','ASC')->get();
    	$ethnic 		 	= Ethnics::orderby('id','ASC')->get();
    	$job_sector 		= Job_sectors::orderby('id','ASC')->get();
    	$mailing_address 	= Mailing_addresses::orderby('id','ASC')->get();
    	$marital 		 	= Marital_status::orderby('id','ASC')->get();
    	$nationality 		= Nationalities::orderby('id','ASC')->get();
    	$naturebusiness 	= NaturalBusinesses::orderby('id','ASC')->get();
    	$occupation 		= Occupations::orderby('id','ASC')->get();
    	$residence 		 	= Recidences::orderby('id','ASC')->get();
    	$relationship 		= Relationships::orderby('id','ASC')->get();
    	$state 		 		= States::orderby('id','ASC')->get();
    	$title 		 		= Titles::orderby('id','ASC')->get();
    	$vip 		 		= Vip::orderby('id','ASC')->get();
        //spouse
        $salutation_spo         = Salutations::orderby('id','ASC')->get();
        $sex_spo                = Sexs::orderby('id','ASC')->get();
        $id_type_spo            = ID_Types::orderby('id','ASC')->get();
        $race_spo               = Races::orderby('id','ASC')->get();
        $religion_spo           = Religions::orderby('id','ASC')->get();
        $ambank_staff_spo       = AmbankStaffs::orderby('id','ASC')->get();
        $applicant_type_spo     = ApplicantTypes::orderby('id','ASC')->get();
        $city_spo               = Cities::orderby('id','ASC')->get();
        $citizenship_spo        = Citizenships::orderby('id','ASC')->get();
        $designation_spo        = Designations::orderby('id','ASC')->get();
        $country_spo            = Countries::orderby('id','ASC')->get();
        $education_spo          = Educations::orderby('id','ASC')->get();
        $employement_type_spo   = Employement_Types::orderby('id','ASC')->get();
        $ethnic_spo             = Ethnics::orderby('id','ASC')->get();
        $job_sector_spo         = Job_sectors::orderby('id','ASC')->get();
        $mailing_address_spo    = Mailing_addresses::orderby('id','ASC')->get();
        $marital_spo            = Marital_status::orderby('id','ASC')->get();
        $nationality_spo        = Nationalities::orderby('id','ASC')->get();
        $naturebusiness_spo     = NaturalBusinesses::orderby('id','ASC')->get();
        $occupation_spo         = Occupations::orderby('id','ASC')->get();
        $residence_spo          = Recidences::orderby('id','ASC')->get();
        $relationship_spo       = Relationships::orderby('id','ASC')->get();
        $state_spo              = States::orderby('id','ASC')->get();
        $title_spo              = Titles::orderby('id','ASC')->get();
        $vip_spo                = Vip::orderby('id','ASC')->get();
        //supplementary
        $salutation_sup         = Salutations::orderby('id','ASC')->get();
        $sex_sup                = Sexs::orderby('id','ASC')->get();
        $id_type_sup            = ID_Types::orderby('id','ASC')->get();
        $race_sup               = Races::orderby('id','ASC')->get();
        $religion_sup           = Religions::orderby('id','ASC')->get();
        $ambank_staff_sup       = AmbankStaffs::orderby('id','ASC')->get();
        $applicant_type_sup     = ApplicantTypes::orderby('id','ASC')->get();
        $city_sup               = Cities::orderby('id','ASC')->get();
        $citizenship_sup        = Citizenships::orderby('id','ASC')->get();
        $designation_sup        = Designations::orderby('id','ASC')->get();
        $country_sup            = Countries::orderby('id','ASC')->get();
        $education_sup          = Educations::orderby('id','ASC')->get();
        $employement_type_sup   = Employement_Types::orderby('id','ASC')->get();
        $ethnic_sup             = Ethnics::orderby('id','ASC')->get();
        $job_sector_sup         = Job_sectors::orderby('id','ASC')->get();
        $mailing_address_sup    = Mailing_addresses::orderby('id','ASC')->get();
        $marital_sup           = Marital_status::orderby('id','ASC')->get();
        $nationality_sup        = Nationalities::orderby('id','ASC')->get();
        $naturebusiness_sup     = NaturalBusinesses::orderby('id','ASC')->get();
        $occupation_sup         = Occupations::orderby('id','ASC')->get();
        $residence_sup          = Recidences::orderby('id','ASC')->get();
        $residence_spo_sup      = Recidences::orderby('id','ASC')->get();
        $relationship_sup       = Relationships::orderby('id','ASC')->get();
        $state_sup              = States::orderby('id','ASC')->get();
        $title_sup             = Titles::orderby('id','ASC')->get();
        $vip_sup                = Vip::orderby('id','ASC')->get();

        //app
        $addon_package      = AddonPackages::orderby('id','ASC')->get();
        $addon_plan         = AddonPlans::orderby('id','ASC')->get();
        $agent_no           = AgentNo::orderby('id','ASC')->get();
        $agent_type         = AgentTypes::orderby('id','ASC')->get();
        $applicant_category = ApplicationCategories::orderby('id','ASC')->get();
        $business_owner     = BusinessOwners::orderby('id','ASC')->get();
        $card_branch        = CardBranches::orderby('id','ASC')->get();
        $card_branch_state  = CardBranchStates::orderby('id','ASC')->get();
        $card_collection    = CardCollections::orderby('id','ASC')->get();
        $card_delivery      = CardDeliveries::orderby('id','ASC')->get();
        $card_type          = CardTypes::orderby('id','ASC')->get();
        $fee_code           = FeeCodes::orderby('id','ASC')->get();
        $gift_code          = GiftCodes::orderby('id','ASC')->where('id','27')->get();
        $highrisk_postcode  = HighRiskPostcodes::orderby('id','ASC')->get();
        $introducer         = Introducers::orderby('id','ASC')->get();
        $plastic_data       = PlasticDatas::orderby('id','ASC')->get();
        $programme_code     = ProgrammeCodes::orderby('id','ASC')->get();
        $product_type       = ProgrammeCodes::select('programme_code')->groupby('programme_code')->get();
        $promotion_program  = PromotionPrograms::orderby('id','ASC')->get();
        $sc_branch          = SourceCodeBranches::orderby('id','ASC')->get();
        $sc_channel         = SourceCodeChannels::orderby('id','ASC')->get();

        $today              = date('dmy');
     
        $temp_entry         = TemporaryEntries::where('account',$account)->limit('1')->first();
        $temp_entry_steps6 = TempEntryStep6::latest('created_at')->where('account_id',$account)->limit('1')->first();//
        $cc_reg             = CCRegisters::latest('created_at')->where('account_id_no',$account)->limit('1')->first();//

        return view('public.entry.entry_step6', compact('salutation','sex','id_type','race','religion','ambank_staff','applicant_type','city','citizenship','designation','country','education','employement_type','ethnic','job_sector','mailing_address','marital','nationality','naturebusiness','occupation','residence','relationship','state','title','vip','addon_package','addon_plan','agent_no','agent_type','applicant_category','business_owner','card_branch','card_branch_state','card_collection','card_delivery','card_type','fee_code','gift_code','highrisk_postcode','introducer','plastic_data','promotion_program','sc_branch','sc_channel','today','programme_code','salutation_spo','sex_spo','id_type_spo','race_spo','religion_spo','ambank_staff_spo','applicant_type_spo','city_spo','citizenship_spo','designation_spo','country_spo','education_spo','employement_type','ethnic','job_sector_spo','mailing_address_spo','marital','nationality_spo','naturebusiness_spo','occupation_spo','residence_spo','relationship_spo','state_spo','title_spo','vip_spo','salutation_sup','sex_sup','id_type_sup','race_sup','religion_sup','ambank_staff_sup','applicant_type_sup','city_sup','citizenship_sup','designation_sup_sup','country_sup_sup','education_sup','employement_type_sup','ethnic_sup','job_sector_sup','mailing_address_sup','marital_sup','nationality_sup','naturebusiness_sup','occupation_sup','residence_sup','relationship_sup','state_sup','title_sup','vip_sup','product_type','temp_entry','temp_entry_steps6','cc_reg'
        )); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_step6(Request $request)
    {
    	$user = Auth::User();
      	$account = $request->input('account');

        $temp 	= TemporaryEntries::where('account',$account)->update([
            "temp_tab6_address_1"   		=> $request->address_1,
            "temp_tab6_address_2"   		=> $request->address_2,
            "temp_tab6_address_3"   		=> $request->address_3,
            "temp_tab6_postcode"   			=> $request->postcode,
            "temp_tab6_city"   				=> $request->city,
            "temp_tab6_state"   			=> $request->state,
            "temp_tab6_country"   			=> $request->country,
            "temp_tab6_home_phone"   		=> $request->home_phone,
            "temp_tab6_residence_type"   	=> $request->residence,
            "temp_tab6_year_stay"   		=> $request->year_stay,
            "temp_tab6_month_stay"   		=> $request->month_stay,

		]);

        return redirect('/data_entry/step_7/'.$account);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {      
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::User();
      	$account = $request->input('account');
      	$sc_channel = $request->input('sc_channel');

        $temp = TemporaryEntries::where('account',$account)->update([
                    "tab1_sc_channel_code"   => $sc_channel
                    ]);

        return redirect('/data_entry/step_1')->with('message','Successfuly Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
