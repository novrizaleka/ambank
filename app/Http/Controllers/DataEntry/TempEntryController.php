<?php

namespace App\Http\Controllers\DataEntry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Parameter\Ind\Salutations;
use App\Model\Parameter\Ind\Sexs;
use App\Model\Parameter\Ind\ID_Types;
use App\Model\Parameter\Ind\Titles;
use App\Model\Parameter\Ind\Religions;
use App\Model\Parameter\Ind\Ethnics;
use App\Model\Parameter\Ind\Races;
use App\Model\Parameter\Ind\AmbankStaffs;
use App\Model\Parameter\Ind\ApplicantTypes;
use App\Model\Parameter\Ind\Cities;
use App\Model\Parameter\Ind\Citizenships;
use App\Model\Parameter\Ind\Countries;
use App\Model\Parameter\Ind\Designations;
use App\Model\Parameter\Ind\Educations;
use App\Model\Parameter\Ind\Employement_Types;
use App\Model\Parameter\Ind\Job_sectors;
use App\Model\Parameter\Ind\Mailing_addresses;
use App\Model\Parameter\Ind\Marital_status;
use App\Model\Parameter\Ind\Nationalities;
use App\Model\Parameter\Ind\Occupations;
use App\Model\Parameter\Ind\NaturalBusinesses;
use App\Model\Parameter\Ind\Recidences;
use App\Model\Parameter\Ind\Relationships;
use App\Model\Parameter\Ind\States;
use App\Model\Parameter\Ind\Vip;
use App\Model\Parameter\App\AddonPackages;
use App\Model\Parameter\App\AddonPlans;
use App\Model\Parameter\App\AgentNo;
use App\Model\Parameter\App\AgentTypes;
use App\Model\Parameter\App\ApplicationCategories;
use App\Model\Parameter\App\BusinessOwners;
use App\Model\Parameter\App\CardBranches;
use App\Model\Parameter\App\CardBranchStates;
use App\Model\Parameter\App\CardCollections;
use App\Model\Parameter\App\CardDeliveries;
use App\Model\Parameter\App\CardTypes;
use App\Model\Parameter\App\FeeCodes;
use App\Model\Parameter\App\GiftCodes;
use App\Model\Parameter\App\HighRiskPostcodes;
use App\Model\Parameter\App\Introducers;
use App\Model\Parameter\App\PlasticDatas;
use App\Model\Parameter\App\ProgrammeCodes;
use App\Model\Parameter\App\PromotionPrograms;
use App\Model\Parameter\App\SourceCodeBranches;
use App\Model\Parameter\App\SourceCodeChannels;

class TempEntryController extends Controller
{
    //
}
