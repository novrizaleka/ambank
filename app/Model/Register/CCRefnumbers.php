<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CCRefnumbers extends Model
{
    protected $table = 'register_cc_ref_numbers';
    protected $fillable = [
       'refno','date_registration','user_id'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
     
}
