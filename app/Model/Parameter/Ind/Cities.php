<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table = 'ind_cities';
}
