<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ID_Types extends Model
{
     protected $table = 'ind_id_types';
    protected $fillable = [
       'id_type_code','id_type_desc','is_active'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
