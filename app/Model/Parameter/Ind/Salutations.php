<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salutations extends Model
{
    protected $table = 'ind_salutations';
    protected $fillable = [
       'salutation_code','salutation_desc','is_active'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}