<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Marital_status extends Model
{
    protected $table = 'ind_marital_status';
}
