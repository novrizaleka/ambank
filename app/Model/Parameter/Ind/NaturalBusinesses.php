<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class NaturalBusinesses extends Model
{
    protected $table = 'ind_nature_of_businesses';
}
