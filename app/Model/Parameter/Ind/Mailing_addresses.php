<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Mailing_addresses extends Model
{
    protected $table = 'ind_mailing_addresses';
}
