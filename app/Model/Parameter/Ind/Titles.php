<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Titles extends Model
{
    protected $table = 'ind_titles';
}
