<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Designations extends Model
{
    protected $table = 'ind_designations';
}
