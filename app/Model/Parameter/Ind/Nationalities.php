<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Nationalities extends Model
{
    protected $table = 'ind_nationalities';
}
