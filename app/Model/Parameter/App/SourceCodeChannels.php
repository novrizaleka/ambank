<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class SourceCodeChannels extends Model
{
    protected $table = 'app_source_code_channels';
}
