<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class FeeCodes extends Model
{
    protected $table = 'app_fee_codes';
}
