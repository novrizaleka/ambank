<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class TypeProducts extends Model
{
   protected $table = 'app_type_products';
}
