<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class AgentTypes extends Model
{
    protected $table = 'app_agent_types';
}
