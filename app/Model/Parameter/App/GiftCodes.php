<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class GiftCodes extends Model
{
    protected $table = 'app_gift_codes';
}
