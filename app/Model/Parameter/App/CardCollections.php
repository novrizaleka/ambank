<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class CardCollections extends Model
{
    protected $table = 'app_card_collections';
}
