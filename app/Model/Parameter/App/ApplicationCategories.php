<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class ApplicationCategories extends Model
{
   protected $table = 'app_application_categories';
}
