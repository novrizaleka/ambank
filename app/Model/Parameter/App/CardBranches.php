<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class CardBranches extends Model
{
    protected $table = 'app_card_branches';
}
