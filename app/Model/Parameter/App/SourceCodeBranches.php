<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class SourceCodeBranches extends Model
{
    protected $table = 'app_source_code_branches';
    protected $fillable = [
       'source_code_branche_code'
    ];

     protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
