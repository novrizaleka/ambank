<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class CardBranchStates extends Model
{
    protected $table = 'app_card_branch_states';
}
