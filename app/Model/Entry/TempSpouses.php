<?php

namespace App\Model\Entry;

use Illuminate\Database\Eloquent\Model;

class TempSpouses extends Model
{
    protected $table 	= 'temp_spouses';
    protected $fillable = [
    ];

    protected $guarded 	= ["id"]; 
    protected $dates 	= ['deleted_at','created_at'];
    public $timestamps 	= true;

    public function ccreg()
    {
        return $this->belongsTo('App\Model\Register\CCRegister', 'id_register', 'id');
    }
}
