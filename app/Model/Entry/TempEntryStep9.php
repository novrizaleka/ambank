<?php

namespace App\Model\Entry;

use Illuminate\Database\Eloquent\Model;

class TempEntryStep9 extends Model
{
    protected $table 	= 'temp_entry_steps9';
    protected $fillable = [
    ];

    protected $guarded 	= ["id"]; 
    protected $dates 	= ['deleted_at','created_at'];
    public $timestamps 	= true;

    public function ccreg()
    {
        return $this->belongsTo('App\Model\Register\CCRegisters', 'id_register', 'id');
    }
}
