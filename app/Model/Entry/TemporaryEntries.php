<?php

namespace App\Model\Entry;

use Illuminate\Database\Eloquent\Model;

class TemporaryEntries extends Model
{
    protected $table = 'main_temporary_entries';
    protected $fillable = [
       'account','tab1_sc_channel_code'
    ];

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at','created_at'];
    public $timestamps = true;
}
