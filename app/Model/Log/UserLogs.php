<?php
namespace App\Model\Log;

use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model
{
     protected $table = 'user_logs';
}
